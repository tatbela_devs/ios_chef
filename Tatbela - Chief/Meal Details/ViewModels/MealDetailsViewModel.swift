//
//  MealDetailsViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class MealDetailsViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared

    // MARK: - Properties
    var mealId: Int? { didSet { if let id = mealId { getMeal(id) } } }
    var meal = BehaviorSubject<Meal?>(value: nil)
    var reviews = BehaviorSubject<[MealReview]>(value: [])
    
    private func getMeal(_ id: Int) {
        networkLayer.getMeal(id, success: { [weak self] meal in
            self?.meal.onNext(meal)
            self?.reviews.onNext(meal.reviews ?? [])
            }, failure: { [weak self] in
                self?.meal.onNext(nil)
                self?.reviews.onNext([])
        })
    }
}
