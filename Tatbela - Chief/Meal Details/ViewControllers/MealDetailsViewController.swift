//
//  MealDetailsViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MealDetailsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var navigationBar: HomeNavigationBarView!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Properties
    let disposeBag = DisposeBag()
    let viewModel = MealDetailsViewModel()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            // Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                tableView.tableHeaderView = headerView
            }
        }
    }
    
    // MARK: - Navigation Bar Setup
    private func setupNavigationBar() {
        viewModel.meal.asDriver(onErrorJustReturn: nil)
            .drive(onNext: { [weak self] meal in
                if let meal = meal, let name = meal.name {
                    self?.navigationBar.title = name
                } else {
                    self?.navigationBar.title = ""
                }
            }).disposed(by: disposeBag)
        navigationBar.title = ""
        navigationBar.leftButtonImage = #imageLiteral(resourceName: "BackIconGrey")
        navigationBar.rightButtonImage = nil
        // handle side menu
        navigationBar.leftButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }).disposed(by: disposeBag)
        // TODO: edit meal will be implemented when the design is changed
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "MealReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "Review")
        let mealDetailsTableViewHeader = MealDetailsTableViewHeader()
        viewModel.meal.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { meal in
                mealDetailsTableViewHeader.setup(meal)
            }).disposed(by: disposeBag)
        tableView.tableHeaderView = mealDetailsTableViewHeader
        tableView.tableFooterView = UIView()
        viewModel.reviews.asDriver(onErrorJustReturn: []).drive(tableView.rx.items(cellIdentifier: "Review", cellType: MealReviewsTableViewCell.self)) { (indexPath, review: MealReview, cell: MealReviewsTableViewCell) -> Void in
            cell.setup(review)
            }.disposed(by: disposeBag)
    }

}
