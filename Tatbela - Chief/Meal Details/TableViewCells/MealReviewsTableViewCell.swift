//
//  MealReviewsTableViewCell.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import SDWebImage

class MealReviewsTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var avatarImage: AvatarImage!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setup(_ review: MealReview) {
        if let image = review.userImageURL {
            avatarImage.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "DefaultAvatarImage"), options: SDWebImageOptions.progressiveDownload, completed: nil)
        }
        nameLabel.text = review.userName
        datetimeLabel.text = review.date
        ratingControl.rating = review.rating ?? 0
        let paragraphStyleAttr = NSMutableParagraphStyle()
        paragraphStyleAttr.lineSpacing = 8
        let textColor = UIColor(hex: 0xB9B9B9)
        let textFont = UIFont(name: "PFDinTextArabic-Regular", size: 15)!
        let attrString = NSAttributedString(string: review.desc ?? "", attributes: [
            .font : textFont,
            .paragraphStyle : paragraphStyleAttr,
            .foregroundColor : textColor
            ])
        descriptionLabel.attributedText = attrString
    }
    
}
