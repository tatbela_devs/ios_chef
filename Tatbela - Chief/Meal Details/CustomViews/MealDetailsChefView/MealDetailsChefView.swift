//
//  MealDetailsChefView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import SDWebImage

@IBDesignable class MealDetailsChefView: UIView {
    let currency = "EGP"

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var avatarImage: AvatarImage!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MealDetailsChefView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup(_ meal: Meal) {
        if let image = meal.chef?.image {
            avatarImage.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "DefaultAvatarImage"), options: SDWebImageOptions.progressiveDownload, completed: nil)
        }
        nameLabel.text = meal.chef?.name
        addressLabel.text = meal.chef?.address?.desc
        ratingControl.rating = meal.rating ?? 0
        if let price = meal.price {
            priceLabel.isHidden = false
            let textColor = UIColor.white
            let priceFont = UIFont(name: Font.medium.rawValue, size: 33)!
            let currencyFont = UIFont(name: Font.regular.rawValue, size: 21)!
            let priceAttrString = NSMutableAttributedString(string: "\(price)", attributes: [
                .font : priceFont,
                .foregroundColor : textColor
                ])
            let currencyAttrString = NSAttributedString(string: "\(currency)", attributes: [
                .font : currencyFont,
                .foregroundColor : textColor
                ])
            let attrString = NSMutableAttributedString(attributedString: priceAttrString)
            attrString.append(currencyAttrString)
            priceLabel.attributedText = attrString
        } else {
            priceLabel.isHidden = true
        }
    }

}
