//
//  MealDetailsImageView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import SDWebImage

class MealDetailsImageView: UIView {
    
    // MARK: - Properties
    var image: String? = nil {
        didSet {
            if let image = image {
                imageView.isHidden = false
                shadowView.isHidden = false
                imageView.sd_setImage(with: URL(string: image), placeholderImage: nil, options: SDWebImageOptions.progressiveDownload, completed: nil)
            } else {
                imageView.isHidden = true
                shadowView.isHidden = true
            }
        }
    }

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var shadowView: ShadowView!
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var gradientView: GradientView!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MealDetailsImageView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    private func setup() {
        roundedView.radius = 19
        shadowView.radius = 19
        shadowView.shadowRadius = 10
        shadowView.shadowOpacity = 0.31
        gradientView.startColor = .clear
        gradientView.endColor = .black
        gradientView.backgroundColor = .clear
    }

}
