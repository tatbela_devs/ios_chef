//
//  MealDetailsTableViewHeader.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MealDetailsTableViewHeader: UIView {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var chefView: MealDetailsChefView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var chefLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mealDescriptionLabel: UILabel!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MealDetailsTableViewHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    // MARK: - Methods
    func setup(_ meal: Meal) {
        view.isHidden = false
        setupPageControl(meal)
        setupScrollView(meal)
        setupChefView(meal)
        setupMealDetails(meal)
    }
    
    private func setupPageControl(_ meal: Meal) {
        if let images = meal.images, images.count > 0 {
            pageControl.isHidden = false
            pageControl.numberOfPages = images.count
            pageControl.currentPage = 0
        } else {
            pageControl.isHidden = true
        }
        
        pageControl.rx.controlEvent(.valueChanged)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                let x = CGFloat(strongSelf.pageControl.currentPage) * strongSelf.scrollView.frame.size.width
                strongSelf.scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
            }).disposed(by: disposeBag)
    }
    
    private func setupScrollView(_ meal: Meal) {
        if let images = meal.images, images.count > 0 {
            scrollView.isHidden = false
            for (index, image) in images.enumerated() {
                var calculatedFrame: CGRect = CGRect(x:0, y:0, width:0, height:0)
                calculatedFrame.origin.x = scrollView.frame.size.width * CGFloat(index)
                calculatedFrame.size = scrollView.frame.size
                let mealDetailsImageView = MealDetailsImageView(frame: calculatedFrame)
                mealDetailsImageView.image = image
                scrollView.addSubview(mealDetailsImageView)
            }
            scrollView.isPagingEnabled = true
            scrollView.contentSize = CGSize(
                width: scrollView.frame.size.width * CGFloat(images.count),
                height: scrollView.frame.size.height)
        } else {
            scrollView.isHidden = true
        }
        
        scrollView.rx.didEndDecelerating
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                let pageNumber = round(strongSelf.scrollView.contentOffset.x / strongSelf.scrollView.frame.size.width)
                strongSelf.pageControl.currentPage = Int(pageNumber)
            }).disposed(by: disposeBag)
    }
    
    private func setupChefView(_ meal: Meal) {
        chefView.setup(meal)
    }
    
    private func setupMealDetails(_ meal: Meal) {
        categoryLabel.text = meal.category?.name
        chefLabel.text = meal.chef?.name
        if let prepareTime = meal.prepareTime {
            timeLabel.text = "\(prepareTime)Min"
        } else {
            timeLabel.text = ""
        }
        let paragraphStyleAttr = NSMutableParagraphStyle()
        paragraphStyleAttr.lineSpacing = 8
        let textColor = UIColor(hex: 0xB9B9B9)
        let textFont = UIFont(name: Font.regular.rawValue, size: 20)!
        let attrString = NSAttributedString(string: meal.desc ?? "", attributes: [
            .font : textFont,
            .paragraphStyle : paragraphStyleAttr,
            .foregroundColor : textColor
            ])
        mealDescriptionLabel.attributedText = attrString
    }
}
