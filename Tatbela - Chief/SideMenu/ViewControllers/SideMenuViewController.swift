//
//  SideMenuViewController.swift
//  Tatbela
//
//  Created by Daniel on 8/3/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit


class SideMenuViewController: UIViewController {
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var mainMenu: UIStackView!
    
    @IBOutlet weak var settingsMenu: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()

        userName.text = LoginResponseModel.getSaved()?.username
        
    }

    @IBAction func settingsClickHandler(_ sender: UIButton) {
        
        self.mainMenu.isHidden = true
        self.settingsMenu.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "logout"{
            LoginResponseModel.removeUser()
        }
    }
}
