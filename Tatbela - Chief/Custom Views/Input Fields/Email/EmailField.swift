//
//  EmailField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class EmailField: UIView {
    
    // MARK: - Inspectables
    @IBInspectable var showIcon: Bool = true { didSet { icon.isHidden = !showIcon } }
    @IBInspectable var fieldName: String = "Email" { didSet { label.text = fieldName } }
    @IBInspectable var placeholder: String = "name@mail.com" { didSet { textField.placeholder = placeholder } }
    
    // MARK: - DisposeBag
    private let disposeBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "EmailField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        // clear text on tap
        button.rx.tap.subscribe(onNext: { [weak self] in
            self?.textField.text = ""
        }).disposed(by: disposeBag)
    }
}
