//
//  InputFieldUnique.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class InputFieldUnique: UIView {
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Inspectables
    @IBInspectable var title: String = "" { didSet { titleLabel.text = title } }
    @IBInspectable var placeholder: String = "" { didSet { textField.placeholder = placeholder } }
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private var isLoading = BehaviorSubject<Bool>(value: false)
    var isUnique = BehaviorSubject<Bool?>(value: nil)
    var uniqueFunction: ((String, @escaping (Bool) -> (), @escaping VoidBlock) -> ())?
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "InputFieldUnique", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        activityIndicator.isHidden = true
        // bind activity indicator animation
        isLoading.asDriver(onErrorJustReturn: false).drive(activityIndicator.rx.isAnimating).disposed(by: disposeBag)
        // bind icons hidden
        isUnique.asDriver(onErrorJustReturn: nil).drive(onNext: { [weak self] isUnique in
            guard let isUnique = isUnique, let text = self?.textField.text, !text.isEmpty else {
                self?.icon.isHidden = true
                return
            }
            self?.icon.image = isUnique ? #imageLiteral(resourceName: "TIcon") : #imageLiteral(resourceName: "XIcon")
            self?.icon.isHidden = false
        }).disposed(by: disposeBag)
        
        // check if text is unique or not
        textField.rx.text.orEmpty.asObservable()
            .debounce(1.0, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .distinctUntilChanged()
            .do(onNext: { [weak self] text in
                if (text.isEmpty) {
                    self?.isLoading.onNext(false)
                    self?.isUnique.onNext(nil)
                }
            }).filter { !$0.isEmpty }
            .subscribe(onNext: { [weak self] text in
                if let uniqueFunction = self?.uniqueFunction {
                    self?.isLoading.onNext(true)
                    self?.isUnique.onNext(nil)
                    uniqueFunction(text, { [weak self] isUnique in
                        self?.isLoading.onNext(false)
                        self?.isUnique.onNext(isUnique)
                        }, { [weak self] in
                            self?.isLoading.onNext(false)
                            self?.isUnique.onNext(nil)
                    })
                }
            }).disposed(by: disposeBag)

    }
}
