//
//  DropdownInputField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class DropdownInputField: UIView {

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var selectedLabel: UILabel!
    
    // MARK: - Properties
    var title: String = "" { didSet { titleLabel.text = title } }
    var placeholder: String = "" { didSet { setupSelectedLabel() } }
    var selectedItem: String? = nil { didSet { setupSelectedLabel() } }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DropdownInputField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setupSelectedLabel() {
        if let selectedItem = selectedItem {
            selectedLabel.textColor = UIColor(hex: 0x191818)
            selectedLabel.text = selectedItem
        } else {
            selectedLabel.textColor = UIColor(hex: 0xD1D1D1)
            selectedLabel.text = placeholder
        }
    }

}
