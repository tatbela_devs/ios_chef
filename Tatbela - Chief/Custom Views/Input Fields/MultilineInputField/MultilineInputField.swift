//
//  MultilineInputField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class MultilineInputField: UIView {

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    var title: String = "" { didSet { titleLabel.text = title } }
    var placeholder: String = "" { didSet { if isPlaceholder { textView.text = placeholder } } }
    var isSecure: Bool = false { didSet { textView.isSecureTextEntry = isSecure } }
    private var isPlaceholder: Bool = true
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MultilineInputField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    private func setup() {
        addPlaceholder()
        textView.rx.didBeginEditing
            .asDriver()
            .drive(onNext: { [weak self] in
                if let isPlaceholder = self?.isPlaceholder, isPlaceholder {
                    self?.isPlaceholder = false
                    self?.textView.textColor = UIColor(hex: 0x191818)
                    self?.textView.text = nil
                }
            }).disposed(by: disposeBag)
        textView.rx.didEndEditing
            .asDriver()
            .drive(onNext: { [weak self] in
                if let textView = self?.textView, textView.text.isEmpty {
                    self?.addPlaceholder()
                }
            }).disposed(by: disposeBag)
    }
    
    func addPlaceholder() {
        isPlaceholder = true
        textView.textColor = UIColor(hex: 0xD1D1D1)
        textView.text = placeholder
    }
}

extension MultilineInputField: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if isPlaceholder {
            isPlaceholder = false
            textView.textColor = UIColor(hex: 0x191818)
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            addPlaceholder()
        }
    }
}
