//
//  InputFieldWithClear.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class InputFieldWithClear: UIView {
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    
    // MARK: - Inspectables
    @IBInspectable var title: String = "" { didSet { titleLabel.text = title } }
    @IBInspectable var placeholder: String = "" { didSet { textField.placeholder = placeholder } }
    
    // MARK: - DisposeBag
    private let disposeBag = DisposeBag()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "InputFieldWithClear", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        // clear text on tap
        button.rx.tap.subscribe(onNext: { [weak self] in
            self?.textField.text = ""
        }).disposed(by: disposeBag)
    }
}
