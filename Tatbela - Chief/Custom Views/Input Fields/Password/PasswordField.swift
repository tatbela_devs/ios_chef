//
//  PasswordField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class PasswordField: UIView {
    
    // MARK: - Inspectables
    @IBInspectable var showIcon: Bool = true { didSet { icon.isHidden = !showIcon } }
    
    // MARK: - DisposeBag
    private let disposeBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PasswordField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        // show/hide password
        button.rx.tap.subscribe(onNext: { [weak self] in
            self?.textField.isSecureTextEntry.toggle()
        }).disposed(by: disposeBag)
    }
}
