//
//  SimpleInputField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class SimpleInputField: UIView {
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    // MARK: - Properties
    @IBInspectable var title: String = "" { didSet { titleLabel.text = title } }
    @IBInspectable var placeholder: String = "" { didSet { textField.placeholder = placeholder } }
    @IBInspectable var isSecure: Bool = false { didSet { textField.isSecureTextEntry = isSecure } }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SimpleInputField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
