//
//  RoundedView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedView: UIView {
    
    @IBInspectable var radius: CGFloat = 26 { didSet { setNeedsLayout() } }
    @IBInspectable var borderWidth: CGFloat = 0 { didSet { setNeedsLayout() } }
    @IBInspectable var borderColor: UIColor = .clear { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
