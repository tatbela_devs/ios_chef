//
//  LocationPermissionView.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 7/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import UIKit

class LocationPermissionView: UIView{
    
    @IBOutlet var contentView: UIView!
    private static var locationPermissionView: LocationPermissionView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    static func hide(){
        LocationPermissionView.locationPermissionView?.removeFromSuperview()
        LocationPermissionView.locationPermissionView = nil
    }
    static func show(){
        if LocationPermissionView.locationPermissionView == nil{
        LocationPermissionView.locationPermissionView = LocationPermissionView(frame: UIScreen.main.bounds)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(LocationPermissionView.locationPermissionView!)
            window?.bringSubview(toFront: LocationPermissionView.locationPermissionView!)
            
        }
    }
    private func commonInit(){
        let viewFileName: String = "LocationPermissionView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
}
