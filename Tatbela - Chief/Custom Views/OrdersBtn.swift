//
//  HistoryOrdersBtn.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class OrdersBtn: UIView {
    @IBOutlet weak var bgView: UIImageView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var labelView: UILabel!
    var isSelected: Bool = false{
        didSet{
            bgView.isHighlighted = isSelected
            iconView.isHighlighted = isSelected
            labelView.isHighlighted = isSelected
        }
    }

}
