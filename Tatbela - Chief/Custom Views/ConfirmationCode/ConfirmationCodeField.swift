//
//  ConfirmationCodeField.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class ConfirmationCodeField: UIView {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    let pinSubject = BehaviorSubject<String>(value: "")
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var tf1: RoundedTextField!
    @IBOutlet weak var tf2: RoundedTextField!
    @IBOutlet weak var tf3: RoundedTextField!
    @IBOutlet weak var tf4: RoundedTextField!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ConfirmationCodeField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        
        tf1.delegate = self
        tf2.delegate = self
        tf3.delegate = self
        tf4.delegate = self
        tf1.clearsOnInsertion = true
        tf2.clearsOnInsertion = true
        tf3.clearsOnInsertion = true
        tf4.clearsOnInsertion = true
        
        tf1.rx.controlEvent([.editingChanged, .editingDidEndOnExit]).asDriver().drive(onNext: { [weak self] in
            self?.tf2.becomeFirstResponder()
        }).disposed(by: disposeBag)
        tf2.rx.controlEvent([.editingChanged, .editingDidEndOnExit]).asDriver().drive(onNext: { [weak self] in
            self?.tf3.becomeFirstResponder()
        }).disposed(by: disposeBag)
        tf3.rx.controlEvent([.editingChanged, .editingDidEndOnExit]).asDriver().drive(onNext: { [weak self] in
            self?.tf4.becomeFirstResponder()
        }).disposed(by: disposeBag)
        tf4.rx.controlEvent(.editingDidEndOnExit).asDriver().drive(onNext: { [weak self] in
            self?.tf4.resignFirstResponder()
        }).disposed(by: disposeBag)
        
        // bind field value
        Observable.combineLatest(
            tf1.rx.text.orEmpty.asObservable(),
            tf2.rx.text.orEmpty.asObservable(),
            tf3.rx.text.orEmpty.asObservable(),
            tf4.rx.text.orEmpty.asObservable()
            ).map { (v1, v2, v3, v4) -> String in
                guard !v1.isEmpty && !v2.isEmpty && !v3.isEmpty && !v4.isEmpty else {
                    return ""
                }
                return "\(v1)\(v2)\(v3)\(v4)"
            }.asDriver(onErrorJustReturn: "")
            .drive(pinSubject)
            .disposed(by: disposeBag)
    }
    
}

extension ConfirmationCodeField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // only 1 character allowed in each text field
        if string.count > 1 || range.location > 0 {
            return false
        } else {
            return true
        }
    }
}
