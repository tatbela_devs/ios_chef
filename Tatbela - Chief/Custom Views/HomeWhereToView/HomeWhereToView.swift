//
//  HomeWhereToView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

@IBDesignable class HomeWhereToView: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var arrowIcon: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    var locationName = BehaviorRelay<String>(value: "")
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HomeWhereToView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup() {
        locationName.asDriver()
            .drive(onNext: { [weak self] text in
                if text.isEmpty {
                    self?.locationLabel.text = "Where to?"
                    self?.locationLabel.textColor = UIColor.grey5
                    self?.arrowIcon.isHidden = false
                } else {
                    self?.locationLabel.text = text
                    self?.locationLabel.textColor = UIColor.black1
                    self?.arrowIcon.isHidden = true
                }
        }).disposed(by: disposeBag)
   }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = 12
    }

}
