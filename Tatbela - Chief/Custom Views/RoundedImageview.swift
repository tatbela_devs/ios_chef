//
//  RoundedView.swift
//  Tatbela
//
//  Created by Daniel on 7/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedImageview: UIImageView {
    
    @IBInspectable var radius: CGFloat = 26 { didSet { setNeedsLayout() } }
    @IBInspectable var borderWidth: CGFloat = 0 { didSet { setNeedsLayout() } }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.masksToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
