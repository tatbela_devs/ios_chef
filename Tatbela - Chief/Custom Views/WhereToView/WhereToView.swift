//
//  WhereToView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

@IBDesignable class WhereToView: UIView {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var xImageView: UIImageView!
    @IBOutlet weak var textField: UITextField!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "WhereToView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = 12
    }
    
    private func setup() {
        // add arrow to the left
        textField.leftView = UIImageView(image: #imageLiteral(resourceName: "Arrow_1"))
        textField.leftViewMode = .unlessEditing

        xImageView.rx.tapGesture()
            .when(.recognized)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.textField.text = ""
            }).disposed(by: disposeBag)
    }
    
}
