//
//  RoundedButtonWithRadius.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/2/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedButtonWithRadius: UIButton {
    
    @IBInspectable var radius: CGFloat = 0 { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = radius
        clipsToBounds = true
    }
}
