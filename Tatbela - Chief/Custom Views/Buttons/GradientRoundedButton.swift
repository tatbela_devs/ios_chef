//
//  GradientRoundedButton.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

// Gradient from left to right
@IBDesignable class GradientRuondedButton: RoundedButton {
    
    // MARK: - Inspectables
    @IBInspectable var startColor: UIColor = .clear { didSet { setNeedsLayout() } }
    @IBInspectable var endColor: UIColor = .clear { didSet { setNeedsLayout() } }
    @IBInspectable var borderColor: UIColor = .clear { didSet { setNeedsLayout() } }
    
    // MARK: - Properties
    let borderWidth: CGFloat = 1
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.borderWidth = borderWidth
        gradientLayer.borderColor = borderColor.cgColor
    }
}
