//
//  ComplainCard.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class ComplainCard: UIView {
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ComplainCard", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup(_ complain: Complain) {
        titleLabel.text = complain.title
        dateLabel.text = complain.date
        let paragraphStyleAttr = NSMutableParagraphStyle()
        paragraphStyleAttr.lineSpacing = 8
        let textColor = UIColor(hex: 0xB9B9B9)
        let textFont = UIFont(name: Font.regular.rawValue, size: 15)!
        let attrString = NSAttributedString(string: complain.desc ?? "", attributes: [
            .font : textFont,
            .paragraphStyle : paragraphStyleAttr,
            .foregroundColor : textColor
            ])
        descriptionLabel.attributedText = attrString
        if let status = complain.status {
            switch status {
            case .inProgress:
                iconImageView.isHidden = false
                statusLabel.isHidden = false
                iconImageView.image = #imageLiteral(resourceName: "OrangeProgressIcon")
                statusLabel.textColor = UIColor(hex: 0xEDB00A)
                statusLabel.text = "In progress"
            case .resolved:
                iconImageView.isHidden = false
                statusLabel.isHidden = false
                iconImageView.image = #imageLiteral(resourceName: "TIcon")
                statusLabel.textColor = UIColor(hex: 0x88B162)
                statusLabel.text = "Resolved"
            default:
                iconImageView.isHidden = true
                statusLabel.isHidden = true
            }
        }
    }

}
