//
//  HomeNavigationBarView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/14/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class HomeNavigationBarView: UIView {
    
    // MARK: - Inspectables
    @IBInspectable var title: String = "" { didSet { titleLabel.text = title } }
    @IBInspectable var leftButtonImage: UIImage? = nil {
        didSet {
            if let image = leftButtonImage {
                leftButton.setImage(image, for: .normal)
                leftButton.isHidden = false
            } else {
                leftButton.isHidden = true
            }
        }
    }
    @IBInspectable var rightButtonImage: UIImage? = nil {
        didSet {
            if let image = rightButtonImage {
                rightButton.setImage(image, for: .normal)
                rightButton.isHidden = false
            } else {
                rightButton.isHidden = true
            }
        }
    }

    // MARK: - Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HomeNavigationBarView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}
