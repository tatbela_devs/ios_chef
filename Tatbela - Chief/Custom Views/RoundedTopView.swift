//
//  RoundedTopView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/4/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedTopView: UIView {
    
    @IBInspectable var radius: CGFloat = 10 { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}
