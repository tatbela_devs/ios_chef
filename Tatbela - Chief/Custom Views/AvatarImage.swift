//
//  AvatarImage.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class AvatarImage: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }
}
