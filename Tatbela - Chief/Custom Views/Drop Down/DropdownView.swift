//
//  DropdownView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/19/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class DropdownView: UIView {
    
    // MARK: - Inspectables
    @IBInspectable var labelColor: UIColor = UIColor(hex: 0xBCBCBC) { didSet { label.textColor = labelColor } }
    @IBInspectable var dropdownArrowImage: UIImage = #imageLiteral(resourceName: "DownArrowBlack") { didSet { arrowImage.image = dropdownArrowImage } }
    
    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DropdownView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
