//
//  RoundedView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class RoundedViewWithShadow: UIView {
    
    @IBInspectable var radius: CGFloat = 26 { didSet { setNeedsLayout() } }
    @IBInspectable var borderWidth: CGFloat = 0 { didSet { setNeedsLayout() } }
    @IBInspectable var borderColor: UIColor = .clear { didSet { setNeedsLayout() } }
    @IBInspectable var shadowColor: UIColor = .black { didSet { setNeedsLayout() } }
    @IBInspectable var shadowOpacity: Float = 1 { didSet { setNeedsLayout() } }
    @IBInspectable var shadowRadius: CGFloat = 0 { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        
        // Shadow
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = shadowOpacity
        layer.masksToBounds = false
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = shadowRadius
        
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: frame.origin.x + frame.size.width, y: frame.origin.y + frame.size.height - radius))
        shadowPath.addArc(withCenter: CGPoint(x: frame.origin.x + frame.size.width - radius, y: frame.origin.y + frame.size.height - radius), radius: radius, startAngle: 0, endAngle: CGFloat.pi / 2, clockwise: true)
        shadowPath.addLine(to: CGPoint(x: frame.origin.x + radius, y: frame.origin.y + frame.size.height))
        shadowPath.addArc(withCenter: CGPoint(x: frame.origin.x + radius, y: frame.origin.y + frame.size.height - radius), radius: radius, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi, clockwise: true)
        
        layer.shadowPath = shadowPath.cgPath
    }
}
