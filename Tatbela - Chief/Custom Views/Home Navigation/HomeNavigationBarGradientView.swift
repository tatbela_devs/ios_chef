//
//  HomeNavigationBarGradientView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/14/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

// Gradient from top to bottom
@IBDesignable class HomeNavigationBarGradientView: UIView {
    
    // MARK: - Inspectables
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        let gradientLayer = layer as! CAGradientLayer
        let color1 = UIColor.white
        let color2 = UIColor.white.withAlphaComponent(0.81)
        let color3 = UIColor.white.withAlphaComponent(0)
        gradientLayer.colors = [color1.cgColor, color2.cgColor, color3.cgColor]
        gradientLayer.locations = [0, 0.8, 1]
    }
    
}
