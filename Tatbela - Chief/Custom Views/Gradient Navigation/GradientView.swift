//
//  GradientView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

// Gradient from top to bottom
@IBDesignable class GradientView: UIView {

    // MARK: - Inspectables
    @IBInspectable var startColor: UIColor = .red { didSet { setNeedsLayout() } }
    @IBInspectable var endColor: UIColor = .green { didSet { setNeedsLayout() } }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
    }

}
