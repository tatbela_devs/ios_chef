//
//  GradientNavigationView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/4/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@IBDesignable class GradientNavigationView: UIView {
    
    // MARK: - Inspectables
    @IBInspectable var title: String = "" { didSet { titleLabel.text = title } }
    @IBInspectable var startColor: UIColor = .clear { didSet { gradientView.startColor = startColor } }
    @IBInspectable var endColor: UIColor = .clear { didSet { gradientView.endColor = endColor } }
    @IBInspectable var hideBackButton: Bool = false { didSet { backButton.isHidden = hideBackButton } }
    
    // MARK: - Outlets
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Properties
    var delegate: BackableDelegate?
    var disposeBag = DisposeBag()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "GradientNavigationView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    private func setup() {
        backButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.delegate?.didTapBack()
            }).disposed(by: disposeBag)
    }
    
}
