//
//  BackableDelegate.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

protocol BackableDelegate {
    func didTapBack()
}
