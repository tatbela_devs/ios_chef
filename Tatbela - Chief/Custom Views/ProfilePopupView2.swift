//
//  LocationPermissionView.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 7/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ProfilePopupView2: UIView{
    
    @IBOutlet weak var cTf: UITextField!
    
    @IBOutlet weak var newTf: UITextField!
    
    @IBOutlet weak var confirmTf: UITextField!
    
    @IBOutlet weak var cancelBtn: RoundedButton!
    @IBOutlet var contentView: UIView!
    private static var profilePopupView2: ProfilePopupView2?
    let viewModel = ChangePasswordViewModel()
    private var disposeBag = DisposeBag()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    static func hide(){
        ProfilePopupView2.profilePopupView2?.removeFromSuperview()
        ProfilePopupView2.profilePopupView2 = nil
    }
    static func show(){
        if ProfilePopupView2.profilePopupView2 == nil{
        ProfilePopupView2.profilePopupView2 = ProfilePopupView2(frame: UIScreen.main.bounds)
        let window = UIApplication.shared.keyWindow
        window?.addSubview(ProfilePopupView2.profilePopupView2!)
            window?.bringSubview(toFront: ProfilePopupView2.profilePopupView2!)
            
        }
    }
    @IBAction func cancelClick(_ sender: RoundedButton) {
        ProfilePopupView2.hide()
    }
    
    @IBAction func changeClick(_ sender: RoundedButton) {
        if newTf.text! == confirmTf.text!{
            self.viewModel.user.value = LoginModel(password: newTf.text! , oldPassword: cTf.text!)
        }
        
    }
    
    
    private func commonInit(){
        let viewFileName: String = "ProfilePopupView2"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        let greenColor = UIColor(hex: "#60992A")
        self.cancelBtn.layer.borderColor = greenColor.cgColor
        
        viewModel.result.asDriver().drive(onNext: { (res) in
            if let res = res {
                if res.isSucces{
                    PositivePopupView.show()
                }else{
                    
                }
                ProfilePopupView2.hide()
            }
        }).disposed(by: disposeBag)
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
}
