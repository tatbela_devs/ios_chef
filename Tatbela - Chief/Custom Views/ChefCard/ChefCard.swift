//
//  ChefCard.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import SDWebImage

@IBDesignable class ChefCard: UIView {

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet weak var avatarImage: AvatarImage!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ChefCard", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func setup(_ chef: Chef) {
        if let image = chef.image {
            avatarImage.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "DefaultAvatarImage"), options: SDWebImageOptions.progressiveDownload, completed: nil)
        }
        nameLabel.text = chef.name
        addressLabel.text = chef.address?.desc
        ratingControl.rating = chef.rating ?? 0
    }
}
