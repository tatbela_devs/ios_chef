//
//  ShadowView.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

@IBDesignable class ShadowView: UIView {
    
    @IBInspectable var radius: CGFloat = 0 { didSet { setNeedsLayout() } }
    @IBInspectable var shadowRadius: CGFloat = 0 { didSet { setNeedsLayout() } }
    @IBInspectable var shadowOpacity: Float = 1 { didSet { setNeedsLayout() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
        clipsToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = shadowRadius
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath

    }
}
