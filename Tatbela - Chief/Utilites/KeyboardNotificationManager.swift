//
//  KeyboardNotificationManager.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class KeyboardNotificationManager {
    
    // MARK: - Singleton
    static var shared = KeyboardNotificationManager()
    
    func keyboardWillShow(_ notification: Notification, scrollView: UIScrollView?) {
        if let userInfo = notification.userInfo {
            let kbSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
            let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
            scrollView?.contentInset = contentInsets
            scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    func keyboardWillHide(_ notification: Notification, scrollView: UIScrollView?) {
        let contentInsets: UIEdgeInsets = .zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
    
}
