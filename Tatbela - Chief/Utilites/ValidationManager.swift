//
//  Validation.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/5/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift

class ValidationManager {
    
    // MARK: - Singleton
    static let shared = ValidationManager()
    
    func validateEmptyString(_ string: String?) -> Bool {
        guard let string = string else {
            return false
        }
        return string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? false : true
    }
    
    func validateStringLength(_ string: String?, min: Int, max: Int) -> Bool  {
        guard validateEmptyString(string) else {
            return false
        }
        return string!.count >= min && string!.count <= max
    }
    
    func validateStringWithExepression(_ string: String, exepression regEx: String) -> Bool {
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: string)
    }
    
    func validateEmail(_ email: String?) -> Bool {
        guard validateEmptyString(email) else {
            return false
        }
        let emailRegEx = "^[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$"
        return validateStringWithExepression(email!, exepression: emailRegEx)
    }
    
    func validatePhoneNumber(_ phoneNumber: String?) -> Bool {
        guard validateEmptyString(phoneNumber) else {
            return false
        }
        let phoneRegEx = "^[0-9]{5,12}$"
        return validateStringWithExepression(phoneNumber!, exepression: phoneRegEx)
    }
    
    func validateComplexPassword(_ password: String?) -> Bool {
        guard validateEmptyString(password) else {
            return false
        }
        let complexPasswordRegEx = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8}$"
        return validateStringWithExepression(password!, exepression: complexPasswordRegEx)
    }
}
