//
//  HttpManager.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire
import Gloss

class HttpManager {
    
    // MARK: - Singelton
    static let shared = HttpManager()
    
    // MARK: - Base URL
    private let baseURL: String = "https://virtserver.swaggerhub.com/t1184/Tatbela3/1.0.0"
    
    // MARK: - Http Post Request
    func get(url: String, parameters: Parameters?, headers: HTTPHeaders?)  -> Observable<Any> {
        return RxAlamofire.requestJSON(.get, "\(baseURL)/\(url)", parameters: parameters, encoding: URLEncoding.queryString, headers: headers)
            .debug()
            .observeOn(MainScheduler.instance)
            .map { $1 }
    }
    
    // MARK: - Http Post Request
    func post(url: String, parameters: Parameters?, headers: HTTPHeaders?)  -> Observable<(Any)> {
        return RxAlamofire.requestJSON(.post, "\(baseURL)/\(url)", parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .debug()
            .observeOn(MainScheduler.instance)
            .map { $1 }
    }
}
