//
//  GoogleManager.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/16/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces
import Alamofire
import RxAlamofire
import RxSwift
import Gloss

class GoogleManager {
    
    // MARK: - Singelton
    static let manager = GoogleManager()
    
    // MARK: - Properties
    private let APIKey = "AIzaSyDrwvsDA8aS43E3kbb_O4jWI6nUeckyyvg"
    private let disposeBag = DisposeBag()
    
    // MARK: - Init Google Maps / Places
    func initServices() {
        GMSServices.provideAPIKey(APIKey)
        GMSPlacesClient.provideAPIKey(APIKey)
    }

    // MARK: - Get Nearby Locations Search
    func getNearbyLocations(_ query: String, success: @escaping ([LocationResult]) -> (), failure: @escaping VoidBlock) {
        let parameters = [
            "key": APIKey,
            "location": "31.2156,29.9553",
            "language": "en",
            "radius": "5000",
            "keyword": query
        ]
        
        RxAlamofire.requestJSON(.get, "https://maps.googleapis.com/maps/api/place/nearbysearch/json", parameters: parameters, encoding: URLEncoding.queryString, headers: nil)
            .debug()
            .observeOn(MainScheduler.instance)
            .map { $1 }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let searchResults = NearbyLocationsSearchResult(json: json) {
                    if searchResults.status == "OK" {
                        if let results = searchResults.results {
                            success(results)
                        }
                    } else {
                        failure()
                    }
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getCurrentLocation() -> Observable<GMSPlace?> {
        return Observable.create({ observer in
            GMSPlacesClient.shared().currentPlace { (placeLikelihoodList, error) in
                if let placeLikelihoodList = placeLikelihoodList {
                    let likelihoods = placeLikelihoodList.likelihoods
                    if likelihoods.count > 0 {
                        if let placeLikelihood = likelihoods.max(by: { $0.likelihood < $1.likelihood }) {
                            observer.onNext(placeLikelihood.place)
                            observer.onCompleted()
                        }
                    }
                } else {
                    if let error = error {
                        print("getCurrentLocation error: \(error.localizedDescription)")
                    }
                    observer.onNext(nil)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        })
    }
    
    func getAutoComplete(_ query: String) -> Observable<[GMSPlace]> {
        return Observable.create({ observer in
            if let myLocation = LocationManager.manager.location.value {
                let distance: Double = 0.001
                // bounds
                let northEast = CLLocationCoordinate2D(latitude: myLocation.coordinate.latitude + distance, longitude: myLocation.coordinate.longitude + distance)
                let southWest = CLLocationCoordinate2D(latitude: myLocation.coordinate.latitude - distance, longitude: myLocation.coordinate.longitude - distance)
                let bounds = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                // filter
                let filter = GMSAutocompleteFilter()
                filter.country = LocationManager.manager.countryCode
                //            filter.type = .establishment
                GMSPlacesClient.shared().autocompleteQuery(query, bounds: bounds, filter: filter, callback: { (results, error) in
                    var places: [GMSPlace] = []
                    if let results = results {
                        for (index, result) in results.enumerated() {
                            if let placeID = result.placeID {
                                GMSPlacesClient.shared().lookUpPlaceID(placeID, callback: { (place, error) in
                                    if let place = place {
                                        places.append(place)
                                        observer.onNext(places)
                                        if index == results.count - 1 {
                                            observer.onCompleted()
                                        }
                                    } else {
                                        if let error = error {
                                            print("lookUpPlaceID error: \(error.localizedDescription)")
                                        }
                                    }
                                })
                            }
                        }
                    } else {
                        if let error = error {
                            print("getAutoComplete error: \(error.localizedDescription)")
                        }
                        observer.onNext([])
                        observer.onCompleted()
                    }
                })
            } else {
                observer.onNext([])
                observer.onCompleted()
            }
            return Disposables.create()
        })
    }
    
    func getLocation(by coordinates: CLLocationCoordinate2D, success: @escaping ([ReverseGeocodingResult]) -> (), failure: @escaping VoidBlock) {
        let parameters = [
            "key": APIKey,
            "latlng": "\(coordinates.latitude),\(coordinates.longitude)",
            "language": "en",
            "result_type": "premise|street_address"
        ]
        RxAlamofire.requestJSON(.get, "https://maps.googleapis.com/maps/api/geocode/json", parameters: parameters, encoding: URLEncoding.queryString, headers: nil)
            .debug()
            .observeOn(MainScheduler.instance)
            .map { $1 }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let searchResults = ReverseGeocodingSearchResult(json: json) {
                    if searchResults.status == "OK" {
                        if let results = searchResults.results {
                            success(results)
                        }
                    } else {
                        failure()
                    }
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getPlace(by coordinates: CLLocationCoordinate2D) -> Observable<GMSPlace?> {
        return Observable.create({ [weak self] observer in
            if let strongSelf = self {
                strongSelf.getLocation(by: coordinates, success: { results in
                    if results.count > 0 , let placeId = results.first?.placeId {
                        strongSelf.getPlaceDetails(placeId)
                            .subscribe(onNext: { place in
                                observer.onNext(place)
                                observer.onCompleted()
                            }).disposed(by: strongSelf.disposeBag)
                    } else {
                        observer.onNext(nil)
                        observer.onCompleted()
                    }
                    }, failure: {
                        observer.onNext(nil)
                        observer.onCompleted()
                })
            } else {
                observer.onNext(nil)
                observer.onCompleted()
            }
            return Disposables.create()
        })
    }
    
    func getPlaceDetails(_ placeId: String) -> Observable<GMSPlace?> {
        return Observable.create({ observer in
            GMSPlacesClient.shared().lookUpPlaceID(placeId) { (place, error) in
                if let place = place {
                    observer.onNext(place)
                    observer.onCompleted()
                } else {
                    if let error = error {
                        print("getPlaceDetails error: \(error.localizedDescription)")
                    }
                    observer.onNext(nil)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        })
    }
}
