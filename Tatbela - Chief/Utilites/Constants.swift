//
//  Constants.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/3/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

class LeftMenu {
    static let items: [(icon: String, title: String)] = [
        ("i", "Home"),
        ("i", "Our Chefs"),
        ("i", "All Meals"),
        ("i", "My Profile"),
        ("i", "My Addresses"),
        ("i", "My Orders"),
        ("i", "Customer Support"),
        ("i", "Notifications"),
        ("i", "Contact Us"),
        ("i", "About us"),
        ("i", "Logout")
    ]
}
