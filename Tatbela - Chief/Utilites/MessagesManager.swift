//
//  MessagesManager.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

class MessagesManager {
    // MARK: - Singleton
    static var shared = MessagesManager()
    
    var warning: String { return NSLocalizedString("warning", comment: "") }
    var invalidUsername: String { return NSLocalizedString("invalid_username", comment: "") }
    var duplicateUsername: String { return NSLocalizedString("duplicate_username", comment: "") }
    var invalidEmail: String { return NSLocalizedString("invalid_email", comment: "") }
    var emptyCountryCode: String { return NSLocalizedString("empty_country_code", comment: "") }
    var invalidPhoneNumber: String { return NSLocalizedString("invalid_phone_number", comment: "") }
    var invalidPassword: String { return NSLocalizedString("invalid_password", comment: "") }
    var emptyPin: String { return NSLocalizedString("empty_pin", comment: "") }
    var invalidPin: String { return NSLocalizedString("invalid_pin", comment: "") }
    var loginSuccess: String { return NSLocalizedString("login_success", comment: "") }
    var loginFail: String { return NSLocalizedString("login_fail", comment: "") }
    var registerSuccess: String { return NSLocalizedString("register_success", comment: "") }
    var registerFail: String { return NSLocalizedString("register_fail", comment: "") }
    var resetPasswordSuccess: String { return NSLocalizedString("reset_password_success", comment: "") }
    var resetPasswordFail: String { return NSLocalizedString("reset_password_fail", comment: "") }
    var confirmPinSuccess: String { return NSLocalizedString("confirm_pin_success", comment: "") }
    var confirmPinFail: String { return NSLocalizedString("confirm_pin_fail", comment: "") }
    var resendPinSuccess: String { return NSLocalizedString("resend_pin_success", comment: "") }
    var resendPinFail: String { return NSLocalizedString("resend_pin_fail", comment: "") }
    var emptyComplainTitle: String { return NSLocalizedString("empty_copmlain_title", comment: "") }
    var emptyComplainDescription: String { return NSLocalizedString("empty_copmlain_description", comment: "") }
    var noComplainOrderSelected: String { return NSLocalizedString("no_complain_order_selected", comment: "") }
    var submitComplainFail: String { return NSLocalizedString("submit_complain_fail", comment: "") }
    var invalidReceiverName: String { return NSLocalizedString("invalid_receiver_name", comment: "") }
    var duplicateReceiverName: String { return NSLocalizedString("duplicate_receiver_name", comment: "") }
    var invalidBankName: String { return NSLocalizedString("invalid_bank_name", comment: "") }
    var invalidBankCode: String { return NSLocalizedString("invalid_bank_code", comment: "") }
    var invalidBankBranch: String { return NSLocalizedString("invalid_bank_branch", comment: "") }
    var invalidBankAddress: String { return NSLocalizedString("invalid_bank_address", comment: "") }
    var invalidAccountNumber: String { return NSLocalizedString("invalid_account_number", comment: "") }
    var invalidSwiftCode: String { return NSLocalizedString("invalid_swift_code", comment: "") }
    var bankAccountSubmitSuccess: String { return NSLocalizedString("bank_account_submit_success", comment: "") }
    var bankAccountSubmitFail: String { return NSLocalizedString("bank_account_submit_fail", comment: "") }
    var invalidCountry: String { return NSLocalizedString("invalid_country", comment: "") }
    var invalidCity: String { return NSLocalizedString("invalid_city", comment: "") }
    var invalidArea: String { return NSLocalizedString("invalid_area", comment: "") }
    var invalidAddress: String { return NSLocalizedString("invalid_address", comment: "") }
    var invalidFloor: String { return NSLocalizedString("invalid_floor", comment: "") }
    var invalidAppartment: String { return NSLocalizedString("invalid_appartment", comment: "") }
    var invalidDelivery: String { return NSLocalizedString("invalid_delivery", comment: "") }
    var saveNewAddressFail: String { return NSLocalizedString("save_new_address_fail", comment: "") }
}
