//
//  Enums.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

enum UserType {
    case user, chief
}

enum Font: String {
    case xThin = "PFDinTextArabic-XThin"
    case thin = "PFDinTextArabic-Thin"
    case light = "PFDinTextArabic-Light"
    case regular = "PFDinTextArabic-Regular"
    case medium = "PFDinTextArabic-Medium"
    case bold = "PFDinTextArabic-Bold"
}

enum MealTableType: Int {
    case today, all, cancelled
}
