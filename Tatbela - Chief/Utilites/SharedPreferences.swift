//
//  SharedPreferences.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/31/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class SharedPreferences {
    static let userDefaults = UserDefaults.standard
    static var isRTL = false
    
    static func changeLanguage() {
        isRTL.toggle()
        
        let semanticContentAttribute: UISemanticContentAttribute = isRTL ? .forceRightToLeft : .forceLeftToRight
        
        UIView.appearance().semanticContentAttribute = semanticContentAttribute
        UINavigationBar.appearance().semanticContentAttribute = semanticContentAttribute
        UISearchBar.appearance().semanticContentAttribute = semanticContentAttribute
        UITextView.appearance().semanticContentAttribute = semanticContentAttribute
        UITextField.appearance().semanticContentAttribute = semanticContentAttribute
        UILabel.appearance().semanticContentAttribute = semanticContentAttribute
    }
    
    static func getChiefId() -> Int {
        // TODO: this should manage chief id in userdefaults, need to be implemented soon :D
        return 1;
    }
}
