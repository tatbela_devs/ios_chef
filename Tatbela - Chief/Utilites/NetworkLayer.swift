//
//  NetworkLayer.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/24/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift
import SVProgressHUD
import Gloss

typealias VoidBlock = () -> ()

class NetworkLayer {
    
    // MARK: - Singelton
    static let shared = NetworkLayer()
    
    // MARK: - Dispose Bag
    private let disposeBag = DisposeBag()
    
    // MARK: - Headers
    private let defaultHeaders: [String: String] = [
        "Content-Type": "application/json"
    ]
    
    func checkChiefUsernameUnique(_ name: String, success: @escaping (Bool) -> (), failure: @escaping VoidBlock) {
        HttpManager.shared.post(url: "chefs/checkUsername", parameters: ["name": name], headers: defaultHeaders)
            .subscribe(onNext: { json in
                if let json = json as? JSON,
                    let usernameUniqueModel = UniqueModel(json: json),
                    let isUnique = usernameUniqueModel.isUnique {
                    success(isUnique)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }

    func login(_ loginModel: LoginModel, success: @escaping (LoginResponseModel)->(), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/login", parameters: loginModel.toJSON(), headers: defaultHeaders)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let user = LoginResponseModel(json: json) {
                    success(user)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func register(_ registerModel: RegisterationModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/register", parameters: registerModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func forgetPassword(_ forgetPasswordModel: ForgetPasswordModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/forgetPassword", parameters: forgetPasswordModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func confirmPin(_ confirmPinModel: ConfirmPinModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/confirmPin", parameters: confirmPinModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func resendPin(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "chefs/resendPin", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Chef
    func getMyMenu(success: @escaping ([CategoryWithMeal]) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        let chefId = SharedPreferences.getChiefId()
        HttpManager.shared.get(url: "chefs/\(chefId)/myMenu", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let categoriesWithMeals = [CategoryWithMeal].from(jsonArray: json) {
                    success(categoriesWithMeals)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func reactivateCancelledMeal(_ mealId: Int, success: @escaping ([CategoryWithMeal]) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        let chefId = SharedPreferences.getChiefId()
        HttpManager.shared.post(url: "chefs/\(chefId)/reactivateMeal/\(mealId)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let categoriesWithMeals = [CategoryWithMeal].from(jsonArray: json) {
                    success(categoriesWithMeals)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func removeTodayMeal(_ mealId: Int, success: @escaping ([CategoryWithMeal]) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        let chefId = SharedPreferences.getChiefId()
        HttpManager.shared.post(url: "chefs/\(chefId)/removeTodayMeal/\(mealId)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let categoriesWithMeals = [CategoryWithMeal].from(jsonArray: json) {
                    success(categoriesWithMeals)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getCategories(success: @escaping ([Category]) -> (), failure: @escaping ()->Void = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "categories", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let categories = [Category].from(jsonArray: json) {
                    success(categories)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getChef(_ id: Int, success: @escaping (Chef) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "chefs/\(id)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let chef = Chef(json: json) {
                    success(chef)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func registerChief(_ registerModel: RegisterationModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/register", parameters: registerModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Complains
    func getComplains(success: @escaping ([Complain]) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "complains", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let complains = [Complain].from(jsonArray: json) {
                    success(complains)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getComplain(_ id: Int, success: @escaping (Complain) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "complains/\(id)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let complain = Complain(json: json) {
                    success(complain)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }

    func submitComplain(_ complain: Complain, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "complains/submit", parameters: complain.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getMeal(_ id: Int, success: @escaping (Meal) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "meals/\(id)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let meal = Meal(json: json) {
                    success(meal)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getNotifications(success: @escaping ([MyNotification]) -> (), failure: @escaping ()->Void = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "notifications", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let notifications = [MyNotification].from(jsonArray: json) {
                    success(notifications)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Address
    func getAddresses(success: @escaping ([Address]) -> (), failure: @escaping ()->Void = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "addresses", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let addresses = [Address].from(jsonArray: json) {
                    success(addresses)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func saveNewAddress(_ newAddressModel: NewAddressModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "addresses/save", parameters: newAddressModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getCountries(success: @escaping ([Country]) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "countries", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let countries = [Country].from(jsonArray: json) {
                    success(countries)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getCountry(_ id: Int, success: @escaping (Country) -> (), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "countries/\(id)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let country = Country(json: json) {
                    success(country)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getCurrentOrders(success: @escaping ([Order]) -> (), failure: @escaping VoidBlock = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "users/orders/current", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let orders = [Order].from(jsonArray: json) {
                    success(orders)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    func getOldOrders(success: @escaping ([Order]) -> (), failure: @escaping VoidBlock = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "users/orders/old", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? [JSON], let orders = [Order].from(jsonArray: json) {
                    success(orders)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func getOrderDetails(id:Int,success: @escaping (Order) -> (), failure: @escaping VoidBlock = {}) {
        SVProgressHUD.showFull()
        HttpManager.shared.get(url: "users/orders/\(id)", parameters: nil, headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do { try? SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let orderDetails = Order(json: json){
                    success(orderDetails)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }

    // MARK: - Bank Account
    func checkBankReceiverNameUnique(_ name: String, success: @escaping (Bool) -> (), failure: @escaping VoidBlock) {
        HttpManager.shared.post(url: "bankAccount/checkUnique", parameters: ["name": name], headers: defaultHeaders)
            .subscribe(onNext: { json in
                if let json = json as? JSON,
                    let usernameUniqueModel = UniqueModel(json: json),
                    let isUnique = usernameUniqueModel.isUnique {
                    success(isUnique)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    
    func submitBankAccount(_ bankAccountModel: BankAccountSubmitModel, success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "bankAccount/submit", parameters: bankAccountModel.toJSON(), headers: defaultHeaders)
            .observeOn(MainScheduler.instance)
            .do {  SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                success()
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
    func changePassword(_ loginModel: LoginModel, success: @escaping (TatbelaResponse)->(), failure: @escaping VoidBlock) {
        SVProgressHUD.showFull()
        HttpManager.shared.post(url: "chefs/changePassword", parameters: loginModel.toJSON(), headers: defaultHeaders)
            .do {  SVProgressHUD.dismiss() }
            .subscribe(onNext: { json in
                if let json = json as? JSON, let res = TatbelaResponse(json: json) {
                    success(res)
                }
            }, onError: { error in
                failure()
            }).disposed(by: disposeBag)
    }
}
