//
//  InternetCheckingService.swift
//  Tatbela
//
//  Created by Daniel on 7/16/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//


import Foundation
import Reachability
import RxSwift
import RxCocoa
import SwiftSocket


class InternetCheckingService{
    static let instance:InternetCheckingService = InternetCheckingService()
    public var hasInternet:Variable<Bool?> = Variable(nil)
    fileprivate let duration = 15
    fileprivate let pingURL = "www.google.com"
    fileprivate let pingPort:Int32 = 80
    fileprivate weak var timer: Timer?
    
    func start(){
        if timer == nil || timer?.isValid == false{
            timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { [weak self] _ in
                _ = self?.pingInternet()
            }
        }
    }
    fileprivate func pingInternet()->Bool{
        let isV4: Bool = self.getIsV4()
        if isV4{
            let client = TCPClient(address: pingURL, port: pingPort)
            switch client.connect(timeout: 4) {
            case .success:
                if(self.hasInternet.value == nil || !self.hasInternet.value! ){
                    self.hasInternet.value = true
                }
                InternetConnectionView.hide()
                client.close()
                return true;
            case .failure( _):
                if(self.hasInternet.value == nil || self.hasInternet.value!){
                    self.hasInternet.value = false
                }
                InternetConnectionView.show()
                client.close()
                return false
            }
            
        }
        return true
    }
    
    private func getIsV4() -> Bool {
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return true }
        guard let firstAddr = ifaddr else { return true }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        let tok =  countInstances(in: address, of: ":")
                        if (tok > 5){
                            return false
                        }
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return true
    }
    
    private func countInstances(in string:String , of stringToFind: String) -> Int {
        var stringToSearch = string
        var count = 0
        while let foundRange = stringToSearch.range(of: stringToFind, options: .diacriticInsensitive) {
            stringToSearch = stringToSearch.replacingCharacters(in: foundRange, with: "")
            count += 1
        }
        return count
    }
}
