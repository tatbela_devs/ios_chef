//
//  LocationManager.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/23/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCoreLocation

class LocationManager: NSObject {
    
    // MARK: - Singleton
    static let manager = LocationManager()
    
    // MARK: - properties
    private let locationManager = CLLocationManager()
    private var locationPermissionView: LocationPermissionView?
    var location: Variable<CLLocation?> = Variable(nil)
    var countryCode: String? = nil
    private let disposeBag = DisposeBag()
    
    func start() {
        enableLocationServices()
        setup()
    }
    
    func enableLocationServices() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            print("Location access is not determined")
            locationManager.requestAlwaysAuthorization()
        case .restricted, .denied:
            addLocationPermissionView()
        case .authorizedWhenInUse,.authorizedAlways:
            // Enable any of your app's location features
            print("Location access is authorized")
            removeLocationPermissionView()
        }
    }
    
    func setup() {
        let locationDriver = locationManager.rx.location.asDriver(onErrorJustReturn: nil)
        // Handle location
        locationDriver
            .drive(location)
            .disposed(by: disposeBag)
        // Handle Country Code
        locationDriver
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { location in
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(location, completionHandler: { [weak self] (placemarks, error) in
                    self?.countryCode = placemarks?.first?.isoCountryCode
                })
            }).disposed(by: disposeBag)
        // Handle authorization for the location manager.
        locationManager.rx.didChangeAuthorization.asDriver()
            .drive(onNext: { [weak self] (manager, status) in
                switch status {
                case .restricted, .notDetermined, .denied :
                    self?.locationManager.stopUpdatingLocation()
                    self?.addLocationPermissionView()
                    
                case .authorizedAlways, .authorizedWhenInUse:
                    self?.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self?.locationManager.requestAlwaysAuthorization()
                    self?.locationManager.distanceFilter = 50
                    self?.locationManager.startUpdatingLocation()
                    self?.removeLocationPermissionView()
                    print("Location status is OK.")
                }
            }).disposed(by: disposeBag)
    }
    
    func addLocationPermissionView(){
        if let window = UIApplication.shared.keyWindow {
            if let locationPermissionView = locationPermissionView {
                if !window.subviews.contains(locationPermissionView) {
                    window.addSubview(locationPermissionView)
                    window.bringSubview(toFront: locationPermissionView)
                }
            } else {
                locationPermissionView = LocationPermissionView(frame: UIScreen.main.bounds)
                window.addSubview(locationPermissionView!)
                window.bringSubview(toFront: locationPermissionView!)
            }
        }
    }
    
    func removeLocationPermissionView(){
        locationPermissionView?.removeFromSuperview()
    }
    
}

