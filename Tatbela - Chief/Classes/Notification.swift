//
//  Chef.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class MyNotification: Glossy {
    var id: Int?
    var title: String?
    var body: String?
    var date: String?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        title = "title" <~~ json
        body = "body" <~~ json
        date = "date" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "title" ~~> title,
            "body" ~~> body,
            "date" ~~> date
            ])
    }
}
