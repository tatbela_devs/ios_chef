//
//  Category.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Category: Glossy {
    
    var id: Int?
    var icon: String?
    var name: String?
    var color: UIColor?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        icon = "icon" <~~ json
        name = "name" <~~ json
        if let hexString: String = "color" <~~ json {
            color = UIColor(hex: hexString)
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "icon" ~~> icon,
            "name" ~~> name
            ])
    }
}
