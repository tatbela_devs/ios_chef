//
//  ForgetPasswordModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class ForgetPasswordModel: Glossy {
    
    var email: String?
    
    init(email: String) {
        self.email = email
    }
    
    // MARK: - Glossy
    required init?(json: JSON) {
        email = "email" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify(["email" ~~> email])
    }
}
