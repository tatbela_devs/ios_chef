//
//  AddressComponent.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/4/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class AddressComponent: JSONDecodable {
    
    var longName: String?
    var shortName: String?
    var types: [String]?
    
    required init?(json: JSON) {
        longName = "long_name" <~~ json
        shortName = "short_name" <~~ json
        types = "types" <~~ json
    }
}
