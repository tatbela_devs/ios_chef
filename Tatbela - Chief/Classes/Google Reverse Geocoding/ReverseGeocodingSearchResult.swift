//
//  ReverseGeocodingSearchResult.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/4/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class ReverseGeocodingSearchResult: JSONDecodable {
    
    var results: [ReverseGeocodingResult]?
    var status: String?
    
    required init?(json: JSON) {
        results = "results" <~~ json
        status = "status" <~~ json
    }
}
