//
//  ReverseGeocodingResult.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/4/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class ReverseGeocodingResult: JSONDecodable {
    
    var addressComponents: [AddressComponent]?
    var formattedAddress: String?
    var geometry: Geometry?
    var placeId: String?
    var types: [String]?
    
    required init?(json: JSON) {
        addressComponents = "address_components" <~~ json
        formattedAddress = "formatted_address" <~~ json
        geometry = "geometry" <~~ json
        placeId = "place_id" <~~ json
        types = "types" <~~ json
    }
}
