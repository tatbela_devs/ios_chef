//
//  BestMealsAndCategoriesResponseModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/14/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class BestMealsAndCategoriesResponseModel: Glossy {
    
    var meals: [Meal]?
    var categories: [Category]?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        meals = "meals" <~~ json
        categories = "categories" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify(["meals" ~~> meals, "categories" ~~> categories])
    }
}
