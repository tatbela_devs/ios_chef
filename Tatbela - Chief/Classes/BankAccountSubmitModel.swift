//
//  BankAccountSubmitModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/2/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class BankAccountSubmitModel: JSONEncodable {
    
    var receiverName: String?
    var bankName: String?
    var bankCode: String?
    var bankBranch: String?
    var bankAddress: String?
    var accountNumber: String?
    var swiftCode: String?
    
    init(receiverName: String, bankName: String, bankCode: String, bankBranch: String, bankAddress: String, accountNumber: String, swiftCode: String) {
        self.receiverName = receiverName
        self.bankName = bankName
        self.bankCode = bankCode
        self.bankBranch = bankBranch
        self.bankAddress = bankAddress
        self.accountNumber = accountNumber
        self.swiftCode = swiftCode
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "receiverName" ~~> receiverName,
            "bankName" ~~> bankName,
            "bankCode" ~~> bankCode,
            "bankBranch" ~~> bankBranch,
            "bankAddress" ~~> bankAddress,
            "accountNumber" ~~> accountNumber,
            "swiftCode" ~~> swiftCode
            ])
    }
}
