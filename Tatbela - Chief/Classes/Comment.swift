//
//  Comment.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Comment: Glossy {
    
    var id: Int?
    var userImageURL: String?
    var userName: String?
    var isUserCustomerSupport: Bool?
    var date: String?
    var desc: String?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        userImageURL = "userImageURL" <~~ json
        userName = "userName" <~~ json
        isUserCustomerSupport = "isUserCustomerSupport" <~~ json
        date = "date" <~~ json
        desc = "description" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "userImageURL" ~~> userImageURL,
            "userName" ~~> userName,
            "isUserCustomerSupport" ~~> isUserCustomerSupport,
            "date" ~~> date,
            "description" ~~> desc
            ])
    }
}
