//
//  Address.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Address: Glossy {
    var id: Int?
    var lat: Double?
    var lng: Double?
    var desc: String?
    var isOnEditMode = false
    
    init(lat:Double, lng:Double) {
        self.lat = lat
        self.lng = lng
    }
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        lat = "lat" <~~ json
        lng = "lng" <~~ json
        desc = "description" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "lat" ~~> lat,
            "lng" ~~> lng,
            "description" ~~> desc
            ])
    }
}
