//
//  Chef.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Chef: Glossy {
    var id: Int?
    var name: String?
    var address: Address?
    var image: String?
    var meals: [Meal]? = []
    var reviews: [ChefReview]?
    var rating: Int?
    var isExpanded = true
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
        address = "address" <~~ json
        image = "image" <~~ json
        meals = "meals" <~~ json
        reviews = "reviews" <~~ json
        rating = "rating" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "address" ~~> address,
            "image" ~~> image,
            "meals" ~~> meals,
            "rating" ~~> rating
            ])
    }
}
