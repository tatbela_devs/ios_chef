//
//  Meal.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Meal: Glossy {
    var id: Int?
    var image: String?
    var name: String?
    var categoryName: String?
    var desc: String?
    var chefName: String?
    var price: Double?
    var prepareTime: String?
    var isToday: Bool?
    var isCancelled: Bool?
    var rating: Int?
    var images: [String]?
    var reviews: [MealReview]?
    var chef: Chef?
    var category: Category?
    var count: Int?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        image = "image" <~~ json
        name = "name" <~~ json
        categoryName = "categoryName" <~~ json
        chefName = "chefName" <~~ json
        desc = "description" <~~ json
        price = "price" <~~ json
        prepareTime = "prepare_time" <~~ json
        isToday = "isToday" <~~ json
        isCancelled = "isCancelled" <~~ json
        rating = "rating" <~~ json
        images = "images" <~~ json
        reviews = "reviews" <~~ json
        chef = "chef" <~~ json
        category = "category" <~~ json
        count = "count" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "image" ~~> image,
            "name" ~~> name,
            "categoryName" ~~> categoryName,
            "chefName" ~~> chefName,
            "description" ~~> desc,
            "price" ~~> price,
            "prepare_time" ~~> prepareTime,
            "isToday" ~~> isToday,
            "isCancelled" ~~> isCancelled,
            "rating" ~~> rating,
            "count" ~~> count
            ])
    }
}
