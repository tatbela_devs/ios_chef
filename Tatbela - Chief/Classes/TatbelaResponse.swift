//
//  Address.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class TatbelaResponse: Glossy {
    
    var status: String?
    var message: String?
    
    required init?(json: JSON) {
        status = "status" <~~ json
        message = "message" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "status" ~~> status,
            "message" ~~> message
            ])
    }
    
    var isSucces:Bool{
        get{
            return self.status == "success"
        }
    }
}
