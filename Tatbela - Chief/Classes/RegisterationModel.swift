//
//  RegisterationModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/6/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class RegisterationModel: Glossy {
    
    var username: String?
    var email: String?
    var phoneNumber: String?
    var countryCode: String?
    var password: String?
    
    // this initializer is not used
    required init?(json: JSON) {
        username = "username" <~~ json
        email = "email" <~~ json
        phoneNumber = "phoneNumber" <~~ json
        countryCode = "countryCode" <~~ json
        password = "password" <~~ json
    }
    
    init(username: String, email: String, phoneNumber: String, countryCode: String, password: String) {
        self.username = username
        self.email = email
        self.phoneNumber = phoneNumber
        self.countryCode = countryCode
        self.password = password
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "username" ~~> username,
            "email" ~~> email,
            "phoneNumber" ~~> phoneNumber,
            "countryCode" ~~> countryCode,
            "password" ~~> password
            ])
    }
}
