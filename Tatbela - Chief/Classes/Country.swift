//
//  Address.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Country: Glossy {
    var id: Int?
    var name: String?
    var cities: [City] = []
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
        cities = "cities" <~~ json ?? []
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "cities" ~~> cities
            ])
    }
}

class City: Glossy, Equatable  {
    
    var id: Int?
    var name: String?
    var areas: [Area] = []
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
        areas = "areas" <~~ json ?? []
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "areas" ~~> areas
            ])
    }
    
    // Equatable
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.id == rhs.id
    }
}

class Area: Glossy, Equatable  {
    var id: Int?
    var name: String?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name
            ])
    }
    
    // Equatable
    static func == (lhs: Area, rhs: Area) -> Bool {
        return lhs.id == rhs.id
    }
}
