//
//  Complain.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Complain: Glossy {
    
    var id: Int?
    var title: String?
    var date: String?
    var desc: String?
    var status: ComplainStatus?
    var comments: [Comment]?
    
    init() {
        title = ""
        desc = ""
        status = .new
    }
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        title = "title" <~~ json
        date = "date" <~~ json
        desc = "description" <~~ json
        status = "status" <~~ json
        comments = "comments" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "title" ~~> title,
            "date" ~~> date,
            "description" ~~> desc,
            "status" ~~> status,
            "comments" ~~> comments
            ])
    }
}

enum ComplainStatus: Int {
    case new = 1, inProgress, resolved, cancelled
}
