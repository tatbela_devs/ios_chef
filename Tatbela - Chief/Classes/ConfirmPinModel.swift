//
//  ConfirmPinModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class ConfirmPinModel: Glossy {
    
    var pin: String?
    
    init(pin: String) {
        self.pin = pin
    }
    
    // MARK: - Glossy
    required init?(json: JSON) {
        pin = "pin" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify(["pin" ~~> pin])
    }
}
