//
//  PlusCode.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/17/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class PlusCode: JSONDecodable {
    
    var compoundCode: String?
    var globalCode: String?
    
    required init?(json: JSON) {
        compoundCode = "compound_code" <~~ json
        globalCode = "globalCode" <~~ json
    }
}
