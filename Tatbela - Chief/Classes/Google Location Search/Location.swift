//
//  Location.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/16/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Location: JSONDecodable {
    
    var lat: Double?
    var lng: Double?
    
    required init?(json: JSON) {
        lat = "lat" <~~ json
        lng = "lng" <~~ json
    }
}
