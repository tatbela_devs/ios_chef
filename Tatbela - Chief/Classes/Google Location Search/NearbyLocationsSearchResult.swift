//
//  NearbyLocationsSearchResult.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/17/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class NearbyLocationsSearchResult: JSONDecodable {
    
    var htmlAttributions: [Any]?
    var nextPageToken: String?
    var results: [LocationResult]?
    var status: String?
    
    required init?(json: JSON) {
        htmlAttributions = "html_attributions" <~~ json
        nextPageToken = "next_page_token" <~~ json
        results = "results" <~~ json
        status = "status" <~~ json
    }
}
