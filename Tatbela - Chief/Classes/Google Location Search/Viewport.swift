//
//  Viewport.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/17/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Viewport: JSONDecodable {
    
    var northeast: Location?
    var southwest: Location?
    
    required init?(json: JSON) {
        northeast = "northeast" <~~ json
        southwest = "southwest" <~~ json
    }
}
