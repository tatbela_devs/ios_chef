//
//  LocationResult.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/17/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class LocationResult: JSONDecodable {
    
    var geometry: Geometry?
    var icon: String?
    var id: String?
    var name: String?
    var placeId: String?
    var plusCode: PlusCode?
    var rating: Float?
    var reference: String?
    var scope: String?
    var types: [String]?
    var vicinity: String?
    
    required init?(json: JSON) {
        geometry = "geometry" <~~ json
        icon = "icon" <~~ json
        id = "id" <~~ json
        name = "name" <~~ json
        placeId = "place_id" <~~ json
        plusCode = "plus_code" <~~ json
        rating = "rating" <~~ json
        reference = "reference" <~~ json
        scope = "scope" <~~ json
        types = "types" <~~ json
        vicinity = "vicinity" <~~ json
    }
}
