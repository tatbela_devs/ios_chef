//
//  Geometry.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/17/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class Geometry: JSONDecodable {
    
    var bounds: Viewport?
    var location: Location?
    var locationType: String?
    var viewport: Viewport?
    
    required init?(json: JSON) {
        bounds = "bounds" <~~ json
        location = "location" <~~ json
        locationType = "location_type" <~~ json
        viewport = "viewport" <~~ json
    }
}
