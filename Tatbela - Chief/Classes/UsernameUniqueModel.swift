//
//  UniqueModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class UniqueModel: JSONDecodable  {
    var isUnique: Bool?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        isUnique = "is_unique" <~~ json
    }
}
