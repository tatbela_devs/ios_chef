//
//  Chef.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 15/8/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss
enum OrderStatusEnum: Int{
    case IN_PROGRESS=1,SCHEDULED=2,DELIVERED=3,CANCELLED=4
}
class Order: Glossy {
    var id: Int?
    var number: String?
    var chef: Chef?
    var status: OrderStatusEnum?
    var totalPrice: Double?
    var subtotalPrice: Double?
    var shipmentPrice: Double?
    var serviceFees: Double?
    var currency: String?
    var meals: [Meal]? = []
    var count:Int?
    var date: String?
    var paymentMethod: PaymentMethod?
    var deliveryAddress: Address?
    var phoneNumber: String?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        number = "number" <~~ json
        chef = "chef" <~~ json
        totalPrice = "totalPrice" <~~ json
        subtotalPrice = "subtotalPrice" <~~ json
        shipmentPrice = "shipmentPrice" <~~ json
        serviceFees = "serviceFees" <~~ json
        currency = "currency" <~~ json
        meals = "meals" <~~ json
        count = "count" <~~ json
        date = "date" <~~ json
        paymentMethod = "paymentMethod" <~~ json
        deliveryAddress = "deliveryAddress" <~~ json
        phoneNumber = "phoneNumber" <~~ json
        
        if let orderStatus: OrderStatus = "status" <~~ json {
            status = OrderStatusEnum(rawValue: orderStatus.id!)
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "number" ~~> number,
            "chef" ~~> chef,
            "totalPrice" ~~> totalPrice,
            "subtotalPrice" ~~> subtotalPrice,
            "shipmentPrice" ~~> shipmentPrice,
            "serviceFees" ~~> serviceFees,
            "currency" ~~> currency,
            "meals" ~~> meals,
            "count" ~~> count,
            "date" ~~> date,
            "paymentMethod" ~~> paymentMethod,
            "deliveryAddress" ~~> deliveryAddress,
            "phoneNumber" ~~> phoneNumber
            ])
    }
}
class OrderStatus: Glossy {
    var id: Int?
    var name: String?
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name
            ])
    }
}
class PaymentMethod{
    var id: Int?
    var name: String?
    required init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name
            ])
    }
}
