//
//  LoginModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class LoginModel: Glossy {
    
    var email: String?
    var password: String?
    var oldPassword: String?
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    init(password: String, oldPassword: String){
        self.password = password
        self.oldPassword = oldPassword
    }
    // MARK: - Glossy
    required init?(json: JSON) {
        email = "email" <~~ json
        password = "password" <~~ json
        oldPassword = "oldPassword" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify(["email" ~~> email, "password" ~~> password, "oldPassword" ~~> oldPassword])
    }
}
class LoginResponseModel: Glossy {
    static let key = "login_user"
    var username: String?
    var Authorization: String?
    var email: String?
    var phone: String?
    var rating: Double?
    
    init(username: String, Authorization: String) {
        self.username = username
        self.Authorization = Authorization
    }
    
    // MARK: - Glossy
    required init?(json: JSON) {
        username = "username" <~~ json
        Authorization = "Authorization" <~~ json
        email = "email" <~~ json
        phone = "phone" <~~ json
        rating = "rating" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify(["username" ~~> username, "email" ~~> email,"phone" ~~> phone,"Authorization" ~~> Authorization,
                  "rating" ~~> rating
                        ])
    }
    func save(){
        UserDefaults.standard.set(self.toJSON(), forKey: LoginResponseModel.key)
    }
    static func getSaved()->LoginResponseModel?{
        let dict = UserDefaults.standard.dictionary(forKey: key)
        if dict != nil{
            return LoginResponseModel(json: dict!)!
        }
        return nil
    }
    static func removeUser(){
       UserDefaults.standard.set(nil, forKey: LoginResponseModel.key)
    }
}
