//
//  NewAddressModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/2/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class NewAddressModel: JSONEncodable {
    
    var country: String?
    var city: String?
    var area: String?
    var address: String?
    var floor: String?
    var appartment: String?
    var otherAddress: String?
    var delivery: String?
    
    init(country: String, city: String, area: String, address: String, floor: String, appartment: String, otherAddress: String, delivery: String) {
        self.country = country
        self.city = city
        self.area = area
        self.address = address
        self.floor = floor
        self.appartment = appartment
        self.otherAddress = otherAddress
        self.delivery = delivery
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "country" ~~> country,
            "city" ~~> city,
            "area" ~~> area,
            "address" ~~> address,
            "floor" ~~> floor,
            "appartment" ~~> appartment,
            "otherAddress" ~~> otherAddress,
            "delivery" ~~> delivery
            ])
    }
}
