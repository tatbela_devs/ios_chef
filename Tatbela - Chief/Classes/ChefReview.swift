//
//  ChefReview.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 8/7/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class ChefReview: Glossy {
    var id: Int?
    var userImageURL: String?
    var userName: String?
    var date: String?
    var desc: String?
    var rating: Int?
    
    // MARK: - Glossy
    required init?(json: JSON) {
        id = "id" <~~ json
        userImageURL = "userImageURL" <~~ json
        userName = "userName" <~~ json
        date = "date" <~~ json
        desc = "description" <~~ json
        rating = "rating" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "userImageURL" ~~> userImageURL,
            "userName" ~~> userName,
            "date" ~~> date,
            "description" ~~> desc,
            "rating" ~~> rating
            ])
    }
}
