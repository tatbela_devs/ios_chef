//
//  CategoryWithMeals.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/21/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import Gloss

class CategoryWithMeal: JSONDecodable {
    var category: Category?
    var meals: [Meal]? = []
    
    required init?(json: JSON) {
        category = "category" <~~ json
        meals = "meals" <~~ json
    }
}
