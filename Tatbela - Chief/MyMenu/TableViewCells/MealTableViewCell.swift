//
//  MealTableViewCell.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/14/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift

class MealTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    let currency = "EGP"
    let disposeBag = DisposeBag()
    var meal: Meal?
    var cellTapAction: VoidBlock?
    var buttonTapAction: ((Int) -> ())?
    var cellTapSubscription: Disposable?
    var buttonTapSubscription: Disposable?

    // MARK: - Outlets
    @IBOutlet weak var mealImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var chefNameLabel: UILabel!
    @IBOutlet weak var prepareTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var mealView: RoundedViewWithShadow!
    
    // MARK: - Setup
    func setup(_ meal: Meal) {
        self.meal = meal
        if let image = meal.image {
            mealImageView.sd_setImage(with: URL(string:image), placeholderImage: #imageLiteral(resourceName: "temp"), options: SDWebImageOptions.progressiveDownload, completed: nil)
        }
        categoryNameLabel.text = meal.categoryName
        chefNameLabel.text = meal.chefName
        descriptionLabel.text = meal.desc
        nameLabel.text = meal.name
        if let price = meal.price {
            priceLabel.isHidden = false
            let textColor = UIColor(hex: 0x88B162)
            let priceFont = UIFont(name: Font.medium.rawValue, size: 33)!
            let currencyFont = UIFont(name: Font.regular.rawValue, size: 21)!
            let priceAttributedString = NSMutableAttributedString(string: "\(price)", attributes: [.font : priceFont])
            let currencyAttributedString = NSAttributedString(string: currency, attributes: [.font : currencyFont])
            priceAttributedString.append(currencyAttributedString)
            priceAttributedString.addAttribute(.foregroundColor, value: textColor, range: NSMakeRange(0, priceAttributedString.length))
            priceLabel.attributedText = priceAttributedString
        } else {
            priceLabel.isHidden = true
        }
        prepareTimeLabel.text = meal.prepareTime
    }
    
    func setup(_ meal: Meal, type: MealTableType, cellAction: VoidBlock?, buttonAction: ((Int) -> ())?) {
        setup(meal)
        setupMealType(type)
        cellTapAction = cellAction
        buttonTapAction = buttonAction
        setupCellTap()
        setupButtonTap()
    }
    
    private func setupMealType(_ type: MealTableType) {
        switch type {
        case .today:
            button.isHidden = false
            let attributedString = NSMutableAttributedString(string: "Remove from ", attributes: [.font : UIFont(name: Font.regular.rawValue, size: 12)!])
            let boldedAttributedString = NSAttributedString(string: "today's menu", attributes: [.font: UIFont(name: Font.medium.rawValue, size: 12)!])
            attributedString.append(boldedAttributedString)
            attributedString.addAttribute(.foregroundColor, value: UIColor(hex: 0xE4510A), range: NSMakeRange(0, attributedString.length))
            button.setAttributedTitle(attributedString, for: .normal)
        case .cancelled:
            button.isHidden = false
            let attributedString = NSMutableAttributedString(string: "Re-Activate", attributes: [
                .font : UIFont(name: Font.medium.rawValue, size: 12)!,
                .foregroundColor : UIColor(hex: 0x60992A)])
            button.setAttributedTitle(attributedString, for: .normal)
        case .all:
            button.isHidden = true
        }
    }
    
    private func setupCellTap() {
        cellTapSubscription?.dispose()
        cellTapSubscription = mealView.rx.tapGesture()
            .when(.recognized)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.cellTapAction?()
            })
        cellTapSubscription?.disposed(by: disposeBag)
    }
    
    private func setupButtonTap() {
        buttonTapSubscription?.dispose()
        buttonTapSubscription = button.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                if let meal = self?.meal, let mealId = meal.id {
                    self?.buttonTapAction?(mealId)
                }
            })
        buttonTapSubscription?.disposed(by: disposeBag)
    }
}
