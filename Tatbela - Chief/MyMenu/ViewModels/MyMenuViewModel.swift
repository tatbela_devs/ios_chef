//
//  MyMenuViewModel.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/18/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class MyMenuViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Properties
    var allMenu: [[Meal]] = []
    var todayMeals: [[Meal]] = []
    var cancelledMeals: [[Meal]] = []
    var allMenuCategories: [Category] = []
    var todayMealsCategories: [Category] = []
    var cancelledMealsCategories: [Category] = []
    var tableViewNeedsRefresh = BehaviorSubject<Void>(value: ())
    var operationError = BehaviorSubject<String?>(value: nil)
    
    init() {
        getMyMenu()
    }
    
    func getMyMenu() {
        networkLayer.getMyMenu(success: { [weak self] categoriesWithMeals in
            self?.setupMeals(categoriesWithMeals)
            self?.tableViewNeedsRefresh.onNext(())
            }, failure: { [weak self] in
                self?.allMenu = []
                self?.todayMeals = []
                self?.cancelledMeals = []
                self?.tableViewNeedsRefresh.onNext(())
        })
    }
    
    func reactivateCancelledMeal(_ id: Int) {
        print("View Model Reactivate Meal : \(id)")
        networkLayer.reactivateCancelledMeal(id, success: { [weak self] categoriesWithMeals in
            self?.setupMeals(categoriesWithMeals)
            self?.tableViewNeedsRefresh.onNext(())
            }, failure: { [weak self] in
                self?.operationError.onNext("Reactivate cancelled meal failed, please try aggain.")
        })
    }
    
    func removeTodayMeal(_ id: Int) {
        print("View Model Remove Meal : \(id)")
        networkLayer.removeTodayMeal(id, success: { [weak self] categoriesWithMeals in
            self?.setupMeals(categoriesWithMeals)
            self?.tableViewNeedsRefresh.onNext(())
            }, failure: { [weak self] in
                self?.operationError.onNext("Remove today meal failed, please try aggain.")
        })
    }
    
    func setupMeals(_ categoriesWithMeals: [CategoryWithMeal]) {
        // clear all arrays first then add the new values
        self.allMenu = []
        self.todayMeals = []
        self.cancelledMeals = []
        self.allMenuCategories = []
        self.todayMealsCategories = []
        self.cancelledMealsCategories = []
        for categoryWithMeal in categoriesWithMeals {
            if let category = categoryWithMeal.category, let meals = categoryWithMeal.meals, meals.count != 0 {
                // check for all meals that are not cancelled
                let allMeals = meals.filter { $0.isCancelled != nil && !$0.isCancelled! }
                if (allMeals.count != 0) {
                    self.allMenuCategories.append(category)
                    self.allMenu.append(allMeals)
                }
                // check for today meals
                let todayMeals = meals.filter { $0.isToday != nil && $0.isToday! }
                if (todayMeals.count != 0) {
                    self.todayMealsCategories.append(category)
                    self.todayMeals.append(todayMeals)
                }
                // check for cancelled meals
                let cancelledMeals = meals.filter { $0.isCancelled != nil && $0.isCancelled! }
                if (cancelledMeals.count != 0) {
                    self.cancelledMealsCategories.append(category)
                    self.cancelledMeals.append(cancelledMeals)
                }
            }
        }
    }
}
