//
//  MyMenuViewController.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/18/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MyMenuViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var navigationBar: HomeNavigationBarView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    let viewModel = MyMenuViewModel()
    var segmentIndex = 0
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupSegmentControl()
        setupTableView()
    }
    
    // MARK: - Navigation Bar Setup
    private func setupNavigationBar() {
        navigationBar.title = "My Menu"
        navigationBar.leftButtonImage = #imageLiteral(resourceName: "BlackBackArrow")
        navigationBar.rightButtonImage = #imageLiteral(resourceName: "BlackPlusIcon")
        // handle side menu
        navigationBar.leftButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }).disposed(by: disposeBag)
        // handle filter
        navigationBar.rightButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                print("add meal")
                self?.viewModel.getMyMenu()
            }).disposed(by: disposeBag)
    }
    
    private func setupSegmentControl() {
        segmentControl.rx.selectedSegmentIndex
            .asDriver()
            .drive(onNext: { [weak self] index in
                self?.segmentIndex = index
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "MealTableViewCell", bundle: nil), forCellReuseIdentifier: "Meal")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        viewModel.tableViewNeedsRefresh
            .asDriver(onErrorJustReturn: ())
            .drive(onNext: { [weak self] in
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MealDetails" {
            let vc = segue.destination as! MealDetailsViewController
            let meal = sender as! Meal
            vc.viewModel.mealId = meal.id
        }
    }
}

// Mark: - Table View
extension MyMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentIndex == 0 { return viewModel.todayMeals.count }
        else if segmentIndex == 1 { return viewModel.allMenu.count }
        else { return viewModel.cancelledMeals.count }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // all menu doesn't have section, THIS IS WEIRD
        if segmentIndex == 1 {
            return CGFloat.leastNormalMagnitude
        } else {
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // all menu doesn't have section, THIS IS WEIRD
        if segmentIndex == 1 {
            return nil
        } else {
            let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:30))
            let label = UILabel(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:30))
            label.font = UIFont(name: Font.medium.rawValue, size: 15)
            if segmentIndex == 0 { label.text = viewModel.todayMealsCategories[section].name }
            else if segmentIndex == 1 { label.text = viewModel.allMenuCategories[section].name }
            else { label.text = viewModel.cancelledMealsCategories[section].name }
            label.textColor = UIColor(hex: 0xBCBCBC)
            view.addSubview(label)
            view.backgroundColor = UIColor.clear
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentIndex == 0 { return viewModel.todayMeals[section].count }
        else if segmentIndex == 1 { return viewModel.allMenu[section].count }
        else { return viewModel.cancelledMeals[section].count }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Meal", for: indexPath) as! MealTableViewCell
        let meal: Meal
        let mealTableType: MealTableType
        let cellTapAction: VoidBlock?
        let buttonTapAction: ((Int) -> ())?
        if (segmentIndex == 0) {
            meal = viewModel.todayMeals[indexPath.section][indexPath.row]
            mealTableType = .today
            buttonTapAction = { [weak self] mealId in
                let alert = UIAlertController.initTatbelaAlert(title: "Remove?", message: "You want remove this item")
                let rightAction = UIAlertAction(title: "Remove", style: .default, handler: { action in
                    print("Remove")
                    self?.viewModel.removeTodayMeal(mealId)
                })
                let leftAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    print("Cancel")
                })
                alert.addAction(rightAction)
                alert.addAction(leftAction)
                self?.present(alert, animated: true, completion: nil)
            }
        } else if (segmentIndex == 1) {
            meal = viewModel.allMenu[indexPath.section][indexPath.row]
            mealTableType = .all
            buttonTapAction = nil
        } else {
            meal = viewModel.cancelledMeals[indexPath.section][indexPath.row]
            mealTableType = .cancelled
            buttonTapAction = { [weak self] mealId in
                let alert = UIAlertController.initTatbelaAlert(title: "Re-Active?", message: "You want Re-Active this item")
                let rightAction = UIAlertAction(title: "Re-Active", style: .default, handler: { action in
                    print("Re-Active")
                    self?.viewModel.reactivateCancelledMeal(mealId)
                })
                let leftAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    print("Cancel")
                })
                alert.addAction(rightAction)
                alert.addAction(leftAction)
                self?.present(alert, animated: true, completion: nil)
            }
        }
        cellTapAction = { [weak self] in
            self?.performSegue(withIdentifier: "MealDetails", sender: meal)
        }
        cell.setup(meal, type: mealTableType, cellAction: cellTapAction, buttonAction: buttonTapAction)
        return cell
    }
}
