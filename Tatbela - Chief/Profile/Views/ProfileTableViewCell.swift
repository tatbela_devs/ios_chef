//
//  ProfileTableViewCell.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import DropDown
import RxSwift

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet weak var dropDownHolderView1: UIView!
    @IBOutlet weak var d1vLbl: UILabel!
    
    @IBOutlet weak var dropDownHolderView2: UIView!
    @IBOutlet weak var d2vLbl: UILabel!
    
    @IBOutlet weak var dropDownHolderView3: UIView!
    @IBOutlet weak var d3vLbl: UILabel!
    
    @IBOutlet weak var saveBtn: UIButton!
    let disposeBag = DisposeBag()
    
    var countries: [Country] = []
    
    var countrySelectedIndex = 0
    var citySelectedIndex = 0
    var areaSelectedIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func dropDown1Click(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = dropDownHolderView1 // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        if self.countries.count > 0{
            dropDown.dataSource = self.countries.map({ (c) -> String in
                return c.name!
            })
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.countrySelectedIndex = index
            self.updateDropsSelectedValues()
               dropDown.hide()
        }
        
        dropDown.show()
     
    }
    
    @IBAction func dropDown2Click(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = dropDownHolderView2 // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        if self.countries.count > 0{
            dropDown.dataSource = self.countries[self.countrySelectedIndex].cities.map({ (c) -> String in
                return c.name!
            })
            
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.citySelectedIndex = index
            self.updateDropsSelectedValues()
            dropDown.hide()
        }
        
        dropDown.show()
        
    }
    
    @IBAction func dropDown3Click(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = dropDownHolderView3 // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        if self.countries.count > 0{
            dropDown.dataSource = self.countries[self.countrySelectedIndex].cities[self.citySelectedIndex].areas.map({ (a) -> String in
                return a.name!
            })
            
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.d3vLbl.text = item
            self.areaSelectedIndex = index
            dropDown.hide()
        }
        
        dropDown.show()
        
    }
    
    
    @IBAction func saveClick(_ sender: UIButton) {
        ProfilePopupView1.show()
    }
    
    @IBAction func changePasswordClick(_ sender: UIButton) {
        ProfilePopupView2.show()
    }
    func setupCell(viewModel: ProfileViewModel){
        let greyColor = UIColor(hex: "#EAE8E8")
        self.dropDownHolderView1.layer.borderColor = greyColor.cgColor
        self.dropDownHolderView2.layer.borderColor = greyColor.cgColor
        self.dropDownHolderView3.layer.borderColor = greyColor.cgColor
        
        let greenColor = UIColor(hex: "#60992A")
        self.saveBtn.layer.borderColor = greenColor.cgColor
        
        let user = LoginResponseModel.getSaved()
        self.emailLbl.text = user?.email
        self.userNameLbl.text = user?.username
        viewModel.countriess.asDriver().drive(onNext: { (countries) in
            self.countries = countries
            if self.countries.count > 0{
                self.updateDropsSelectedValues()
                
            }
        }).disposed(by: disposeBag)
        
        
    }
    
    private func updateDropsSelectedValues(){
        self.d1vLbl.text = self.countries[self.countrySelectedIndex].name
        self.d2vLbl.text = self.countries[self.countrySelectedIndex].cities[self.citySelectedIndex].name
        self.d3vLbl.text = self.countries[self.countrySelectedIndex].cities[self.citySelectedIndex].areas[self.areaSelectedIndex].name
    }
    
}
