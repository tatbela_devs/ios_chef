//
//  ProfileViewController.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift

enum Mode {
    case PROFILE,PAYMENT
}
class MyProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var ratingController: RatingControl!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var phoneCodeLbl: UILabel!
    
    @IBOutlet weak var phoneNumLbl: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var payIconIV: UIImageView!
    
    @IBOutlet weak var payTextLbl: UILabel!
    
    let viewModel = ProfileViewModel()
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var navigationBar: HomeNavigationBarView!
    
    var mode: Mode = .PROFILE
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        
        self.phoneNumLbl.text = viewModel.user?.phone
        self.nameLbl.text = viewModel.user?.username
        self.ratingController.starCount = 5
        self.ratingController.rating = Int(viewModel.user?.rating ?? 0)
        
    }

    @IBAction func paymentDetails(_ sender: UIButton) {
        self.mode = self.mode == .PROFILE ? .PAYMENT : .PROFILE
        self.tableView.reloadData()
    }
    
    func setupNavigationBar() {
        navigationBar.title = "My Profile"
        navigationBar.leftButtonImage = #imageLiteral(resourceName: "BackIconGrey")
        navigationBar.rightButtonImage = #imageLiteral(resourceName: "CartIcon")
        // handle side menu
        navigationBar.leftButton.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }).disposed(by: disposeBag)
        // handle filter
        navigationBar.rightButton.rx
            .tap
            .asDriver()
            .drive(onNext: {
               
            }).disposed(by: disposeBag)
    }
    
}

extension MyProfileViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if self.mode == .PROFILE{
            cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell")
            (cell as! ProfileTableViewCell).setupCell(viewModel:viewModel)
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell")
        }
        return cell
    }
    
    
}
