//
//  FromToViewModel.swift
//  Tatbela
//
//  Created by Daniel on 7/28/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class ProfileViewModel {
    
    // MARK: - Properties
    var countriess = Variable<[Country]>([])
    private let disposeBag = DisposeBag()
    
    // MARK: - Network
    let networkLayer = NetworkLayer.shared
    
    init() {
        
        self.networkLayer.getCountries(success:{ (countriess) in
            self.countriess.value = countriess
        }, failure: {})
    
    }
    
    var user :LoginResponseModel?{
        get{
            return LoginResponseModel.getSaved()
        }
        
    }
}
