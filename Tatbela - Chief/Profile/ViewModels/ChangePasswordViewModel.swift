//
//  ChangePasswordViewModel.swift
//  Tatbela
//
//  Created by Daniel on 7/28/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class ChangePasswordViewModel {
    
    // MARK: - Properties
    var countriess = Variable<[Country]>([])
    private let disposeBag = DisposeBag()
    private(set) var user =  Variable<LoginModel?>(nil)
    private(set) var result =  Variable<TatbelaResponse?>(nil)
    // MARK: - Network
    let networkLayer = NetworkLayer.shared
    
    init() {
        
        user.asDriver().drive(onNext: { [unowned self] (user) in
            if user != nil{
                self.changePassword()
            }
        }).disposed(by: disposeBag)
    
    }
    func changePassword(){
        self.networkLayer.changePassword(user.value! ,success:{ (res) in
            self.result.value = res
        }, failure: {})
    }
   
}
