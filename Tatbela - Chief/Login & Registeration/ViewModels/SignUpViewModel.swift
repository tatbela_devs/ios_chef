//
//  SignUpViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class SignUpViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - Variables
    let usernameSubject = BehaviorSubject<String>(value: "")
    let usernameUniqueSubject = BehaviorSubject<Bool?>(value: nil)
    let emailSubject = BehaviorSubject<String>(value: "")
    let phoneNumberSubject = BehaviorSubject<String>(value: "")
    let countryCodeSubject = BehaviorSubject<String>(value: "")
    let passwordSubject = BehaviorSubject<String>(value: "")
    let validationErrorMessage = BehaviorSubject<String?>(value: nil)
    // validation
    var isValid: Bool {
        let validationManager = ValidationManager.shared
        // username
        guard validationManager.validateEmptyString(try? usernameSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidUsername)
            return false
        }
        if let tempUnique = try? usernameUniqueSubject.value(), let isUnique = tempUnique, !isUnique {
            validationErrorMessage.onNext(messagesManager.duplicateUsername)
            return false
        }
        // email
        guard validationManager.validateEmail(try? emailSubject.value()) else {
                validationErrorMessage.onNext(messagesManager.invalidEmail)
                return false
        }
        // phone number
        guard validationManager.validatePhoneNumber(try? phoneNumberSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidPhoneNumber)
            return false
        }
        guard validationManager.validateEmptyString(try? countryCodeSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.emptyCountryCode)
            return false
        }
        // password
        guard validationManager.validateComplexPassword(try? passwordSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidPassword)
            return false
        }
        validationErrorMessage.onNext(nil)
        return true
    }
    
    // MARK: - Methods
    func register(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        if isValid {
            if let username = try? usernameSubject.value(),
                let email = try? emailSubject.value(),
                let phoneNumber = try? phoneNumberSubject.value(),
                let countryCode = try? countryCodeSubject.value(),
                let password = try? passwordSubject.value() {
                let registerModel = RegisterationModel(
                    username: username,
                    email: email,
                    phoneNumber: phoneNumber,
                    countryCode: countryCode,
                    password: password)
                networkLayer.register(registerModel, success: {
                    success()
                }, failure: {
                    failure()
                })
            }
        }
    }
}
