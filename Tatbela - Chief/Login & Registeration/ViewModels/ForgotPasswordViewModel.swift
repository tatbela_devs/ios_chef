//
//  ForgotPasswordViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class ForgotPasswordViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - Variables
    let emailSubject = BehaviorSubject<String>(value: "")
    let validationErrorMessage = BehaviorSubject<String?>(value: nil)
    var isValid: Bool {
        let validationManager = ValidationManager.shared
        guard validationManager.validateEmptyString(try? emailSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidEmail)
            return false
        }
        validationErrorMessage.onNext(nil)
        return true
    }
    
    // MARK: - Methods
    func forgetPassword(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        if isValid {
            if let email = try? emailSubject.value() {
                let forgetPasswordModel = ForgetPasswordModel(email: email)
                networkLayer.forgetPassword(forgetPasswordModel, success: {
                    success()
                }, failure: {
                    failure()
                })
            }
        }
    }
}
