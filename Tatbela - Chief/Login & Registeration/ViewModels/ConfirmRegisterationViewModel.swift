//
//  ConfirmRegisterationViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class ConfirmRegisterationViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - Variables
    let pinSubject = BehaviorSubject<String>(value: "")
    let validationErrorMessage = BehaviorSubject<String?>(value: nil)
    var isValid: Bool {
        let validationManager = ValidationManager.shared
        guard validationManager.validateEmptyString(try? pinSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.emptyPin)
            return false
        }
        validationErrorMessage.onNext(nil)
        return true
    }
    
    func confirmPin(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        if isValid {
            if let pin = try? pinSubject.value() {
                let confirmPinModel = ConfirmPinModel(pin: pin)
                networkLayer.confirmPin(confirmPinModel, success: {
                    success()
                }, failure: {
                    failure()
                })
            }
        }
    }
    
    func resendPin(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        networkLayer.resendPin(success: {
            success()
        }, failure: {
            failure()
        })
    }
}
