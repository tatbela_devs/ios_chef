//
//  BankAccountViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import RxSwift

class BankAccountViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - Variables
    let receiverNameSubject = BehaviorSubject<String>(value: "")
    let receiverNameUniqueSubject = BehaviorSubject<Bool?>(value: nil)
    let bankNameSubject = BehaviorSubject<String>(value: "")
    let bankCodeSubject = BehaviorSubject<String>(value: "")
    let bankBranchSubject = BehaviorSubject<String>(value: "")
    let bankAddressSubject = BehaviorSubject<String>(value: "")
    let accountNumberSubject = BehaviorSubject<String>(value: "")
    let swiftCodeSubject = BehaviorSubject<String>(value: "")
    let validationErrorMessage = BehaviorSubject<String?>(value: nil)
    
    // validation
    var isValid: Bool {
        let validationManager = ValidationManager.shared
        // Receiver Name
        guard validationManager.validateEmptyString(try? receiverNameSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidReceiverName)
            return false
        }
        if let tempUnique = try? receiverNameUniqueSubject.value(), let isUnique = tempUnique, !isUnique {
            validationErrorMessage.onNext(messagesManager.duplicateReceiverName)
            return false
        }
        // Bank Name
        guard validationManager.validateEmptyString(try? bankNameSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidBankName)
            return false
        }
        // Bank Code
        guard validationManager.validateEmptyString(try? bankCodeSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidBankCode)
            return false
        }
        // Bank Branch
        guard validationManager.validateEmptyString(try? bankBranchSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidBankBranch)
            return false
        }
        // Bank Address
        guard validationManager.validateEmptyString(try? bankAddressSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidBankAddress)
            return false
        }
        // Account Number
        guard validationManager.validateEmptyString(try? accountNumberSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidAccountNumber)
            return false
        }
        // Swift Code
        guard validationManager.validateEmptyString(try? swiftCodeSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.invalidSwiftCode)
            return false
        }
        validationErrorMessage.onNext(nil)
        return true
    }
    
    // MARK: - Methods
    func submit(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        if isValid {
            if let receiverName = try? receiverNameSubject.value(),
                let bankName = try? bankNameSubject.value(),
                let bankCode = try? bankCodeSubject.value(),
                let bankBranch = try? bankBranchSubject.value(),
                let bankAddress = try? bankAddressSubject.value(),
                let accountNumber = try? accountNumberSubject.value(),
                let swiftCode = try? swiftCodeSubject.value() {
                
                let bankAccountModel = BankAccountSubmitModel(
                    receiverName: receiverName,
                    bankName: bankName,
                    bankCode: bankCode,
                    bankBranch: bankBranch,
                    bankAddress: bankAddress,
                    accountNumber: accountNumber,
                    swiftCode: swiftCode)
                
                networkLayer.submitBankAccount(bankAccountModel, success: {
                    success()
                }, failure: {
                    failure()
                })
                
            }
        }
    }
}

