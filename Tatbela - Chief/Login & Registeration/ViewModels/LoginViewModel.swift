//
//  LoginViewModel.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/29/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel {
    
    // MARK: - Network
    let networkLayer: NetworkLayer = NetworkLayer.shared
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - Variables
    let emailSubject = BehaviorSubject<String>(value: "")
    let passwordSubject = BehaviorSubject<String>(value: "")
    let validationErrorMessage = BehaviorSubject<String?>(value: nil)
    var isValid: Bool {
        let validationManager = ValidationManager.shared
        guard validationManager.validateEmptyString(try? emailSubject.value()) && validationManager.validateEmptyString(try? passwordSubject.value()) else {
            validationErrorMessage.onNext(messagesManager.loginFail)
            return false
        }
        validationErrorMessage.onNext(nil)
        return true
    }
    
    // MARK: - Methods
    func login(success: @escaping VoidBlock, failure: @escaping VoidBlock) {
        if isValid {
            if let email = try? emailSubject.value(), let password = try? passwordSubject.value() {
                let loginModel = LoginModel(email: email, password: password)
                networkLayer.login(loginModel, success: { user in
                    user.save()
                    success()
                }, failure: {
                    failure()
                })
            }
        }
    }
}
