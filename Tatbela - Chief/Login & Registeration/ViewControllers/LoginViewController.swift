//
//  LoginViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/30/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailField: EmailField!
    @IBOutlet weak var passwordField: PasswordField!
    @IBOutlet weak var loginButton: UIButton!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - View Model
    lazy var viewModel = LoginViewModel()
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        setupInputFields()
        setupLoginButtonTap()
        setupViewTap()
        setupValidationMessages()
        setupKeyboardNotifications()
    }
    private func setupInputFields() {
        // email
        emailField.textField.rx.text.orEmpty.asDriver().drive(viewModel.emailSubject).disposed(by: disposeBag)
        // password
        passwordField.textField.rx.text.orEmpty.asDriver().drive(viewModel.passwordSubject).disposed(by: disposeBag)
    }
    private func setupLoginButtonTap() {
        loginButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.login(success: { [weak self] in
                    
                    let vc = UIStoryboard.home.instantiateInitialViewController()
                    self?.showDetailViewController(vc!, sender: nil)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.loginFail)
                })
            }).disposed(by: disposeBag)
    }
    private func setupViewTap() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.rx.event
            .subscribe { [unowned self] _ in
                self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    private func setupValidationMessages() {
        viewModel.validationErrorMessage.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { [weak self] errorMessage in
                self?.showWarningAlert(errorMessage)
            }).disposed(by: disposeBag)
    }
    // MARK: - Keyboard Notification
    private func setupKeyboardNotifications() {
        NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillShow(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(.UIKeyboardDidHide)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillHide(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
    }
}
