//
//  BankAccountNoticeViewController.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

class BankAccountNoticeViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var navigationView: GradientNavigationView!

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        navigationView.delegate = self
    }
}
