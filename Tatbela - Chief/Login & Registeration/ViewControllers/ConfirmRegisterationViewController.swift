//
//  ConfirmRegisterationViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/11/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ConfirmRegisterationViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var confirmationCodeField: ConfirmationCodeField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var navigationView: GradientNavigationView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - View Model
    lazy var viewModel = ConfirmRegisterationViewModel()
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        navigationView.delegate = self
        setupInputFields()
        setupButtonsTap()
        setupViewTap()
        setupValidationMessages()
    }
    private func setupInputFields() {
        confirmationCodeField.pinSubject.asDriver(onErrorJustReturn: "").drive(viewModel.pinSubject).disposed(by: disposeBag)
    }
    private func setupButtonsTap() {
        confirmButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.confirmPin(success: { [weak self]  in
//                    self?.showAlert(title: "", message: self?.messagesManager.confirmPinSuccess)
                     self?.performSegue(withIdentifier: "BankAccount", sender: nil)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.confirmPinFail)
                })
            }).disposed(by: disposeBag)
        
        resendButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.resendPin(success: { [weak self]  in
                    self?.showAlert(title: "", message: self?.messagesManager.resendPinSuccess)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.resendPinFail)
                })
            }).disposed(by: disposeBag)
    }
    private func setupViewTap() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.rx.event
            .subscribe { [unowned self] _ in
                self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    private func setupValidationMessages() {
        viewModel.validationErrorMessage.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { [weak self] errorMessage in
                self?.showWarningAlert(errorMessage)
            }).disposed(by: disposeBag)
    }
    
}
