//
//  BankAccountViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 9/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BankAccountViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var receiverNameField: InputFieldUnique!
    @IBOutlet weak var bankNameField: InputFieldWithClear!
    @IBOutlet weak var bankCodeField: SimpleInputField!
    @IBOutlet weak var bankBranchField: SimpleInputField!
    @IBOutlet weak var bankAddressField: SimpleInputField!
    @IBOutlet weak var accountNumberField: SimpleInputField!
    @IBOutlet weak var swiftCodeField: SimpleInputField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var navigationView: GradientNavigationView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    let viewModel = BankAccountViewModel()
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        navigationView.delegate = self
        setupInputFields()
        setupSubmitButtonTap()
        setupViewTap()
        setupValidationMessages()
        setupKeyboardNotifications()
    }
    private func setupInputFields() {
        // set receiver name unique function
        receiverNameField.uniqueFunction = NetworkLayer.shared.checkBankReceiverNameUnique
        // bind all input fields
        receiverNameField.textField.rx.text.orEmpty.asDriver().drive(viewModel.receiverNameSubject).disposed(by: disposeBag)
        receiverNameField.isUnique.asDriver(onErrorJustReturn: nil).drive(viewModel.receiverNameUniqueSubject).disposed(by: disposeBag)
        bankNameField.textField.rx.text.orEmpty.asDriver().drive(viewModel.bankNameSubject).disposed(by: disposeBag)
        bankCodeField.textField.rx.text.orEmpty.asDriver().drive(viewModel.bankCodeSubject).disposed(by: disposeBag)
        bankBranchField.textField.rx.text.orEmpty.asDriver().drive(viewModel.bankBranchSubject).disposed(by: disposeBag)
        bankAddressField.textField.rx.text.orEmpty.asDriver().drive(viewModel.bankAddressSubject).disposed(by: disposeBag)
        accountNumberField.textField.rx.text.orEmpty.asDriver().drive(viewModel.accountNumberSubject).disposed(by: disposeBag)
        swiftCodeField.textField.rx.text.orEmpty.asDriver().drive(viewModel.swiftCodeSubject).disposed(by: disposeBag)
    }
    private func setupSubmitButtonTap() {
        submitButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.view.endEditing(true)
                self?.viewModel.submit(success: { [weak self] in
                        self?.performSegue(withIdentifier: "Review", sender: nil)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.bankAccountSubmitFail)
                })
            }).disposed(by: disposeBag)
    }
    private func setupViewTap() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.subscribe { [unowned self] _ in
            self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    private func setupValidationMessages() {
        viewModel.validationErrorMessage.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { [weak self] errorMessage in
                self?.showWarningAlert(errorMessage)
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Keyboard Notification
    private func setupKeyboardNotifications() {
        NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillShow(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(.UIKeyboardDidHide)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillHide(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
    }
}
