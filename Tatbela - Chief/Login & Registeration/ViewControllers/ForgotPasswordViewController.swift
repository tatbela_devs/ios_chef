//
//  ForgotPasswordViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 5/31/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailField: EmailField!
    @IBOutlet weak var resetPasswordButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - View Model
    lazy var viewModel = ForgotPasswordViewModel()
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        setupInputFields()
        setupResetPasswordButtonTap()
        setupBackButtonTap()
        setupViewTap()
        setupValidationMessages()
        setupKeyboardNotifications()
    }
    private func setupInputFields() {
        emailField.textField.rx.text.orEmpty.asDriver().drive(viewModel.emailSubject).disposed(by: disposeBag)
    }
    private func setupResetPasswordButtonTap() {
        resetPasswordButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.forgetPassword(success: { [weak self] in
                    self?.showAlert(title: "", message: self?.messagesManager.resetPasswordSuccess)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.resetPasswordFail)
                })
            }).disposed(by: disposeBag)
    }
    private func setupBackButtonTap() {
        backButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let _ = self?.navigationController else {
                    return
                }
                self?.navigationController?.popViewController(animated: true)
            }).disposed(by: disposeBag)
    }
    private func setupViewTap() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.subscribe { [unowned self] _ in
            self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    private func setupValidationMessages() {
        viewModel.validationErrorMessage.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { [weak self] errorMessage in
                self?.showWarningAlert(errorMessage)
            }).disposed(by: disposeBag)
    }
    // MARK: - Keyboard Notification
    private func setupKeyboardNotifications() {
        NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillShow(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(.UIKeyboardDidHide)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillHide(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
    }
}
