//
//  AlmostDoneViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/12/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AlmostDoneViewController: UIViewController {
    
    // MARK: - Properties
    let disposeBag = DisposeBag()

    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addFoodButton: GradientRuondedButton!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // add food button action
        addFoodButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in
                print("Get Start")
            }).disposed(by: disposeBag)

    }
}
