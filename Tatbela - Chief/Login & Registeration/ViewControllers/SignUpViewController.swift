//
//  SignUpViewController.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 6/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift

class SignUpViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var usernameField: InputFieldUnique!
    @IBOutlet weak var emailField: InputFieldWithClear!
    @IBOutlet weak var phoneNumberField: PhoneNumberField!
    @IBOutlet weak var passwordField: PasswordField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var navigationView: GradientNavigationView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - Messages
    let messagesManager: MessagesManager = MessagesManager.shared
    
    // MARK: - View Model
    lazy var viewModel = SignUpViewModel()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup Methods
    private func setup() {
        navigationView.delegate = self
        setupInputFields()
        setupRegisterButtonTap()
        setupViewTap()
        setupValidationMessages()
        setupKeyboardNotifications()
    }
    private func setupInputFields() {
        // set username unique function
        usernameField.uniqueFunction = NetworkLayer.shared.checkChiefUsernameUnique
        // bind all input fields
        // username
        usernameField.textField.rx.text.orEmpty.asDriver().drive(viewModel.usernameSubject).disposed(by: disposeBag)
        usernameField.isUnique.asDriver(onErrorJustReturn: nil).drive(viewModel.usernameUniqueSubject).disposed(by: disposeBag)
        // email
        emailField.textField.rx.text.orEmpty.asDriver().drive(viewModel.emailSubject).disposed(by: disposeBag)
        // phone number
        phoneNumberField.phoneNumberSubject.asDriver(onErrorJustReturn: "").drive(viewModel.phoneNumberSubject).disposed(by: disposeBag)
        phoneNumberField.countryCodeSubject.asDriver(onErrorJustReturn: "").drive(viewModel.countryCodeSubject).disposed(by: disposeBag)
        // password
        passwordField.textField.rx.text.orEmpty.asDriver().drive(viewModel.passwordSubject).disposed(by: disposeBag)
    }

    private func setupRegisterButtonTap() {
        registerButton.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.view.endEditing(true)
                self.viewModel.register(success: { [weak self] in
//                    self?.showAlert(title: "", message: self?.messagesManager.registerSuccess)
                    self?.performSegue(withIdentifier: "ConfirmPin", sender: nil)
                    }, failure: { [weak self] in
                        self?.showWarningAlert(self?.messagesManager.registerFail)
                })
            }).disposed(by: disposeBag)
    }
    private func setupViewTap() {
        let tapGesture = UITapGestureRecognizer()
        view.addGestureRecognizer(tapGesture)
        tapGesture.rx.event
            .subscribe { [unowned self] _ in
                self.view.endEditing(true)
            }.disposed(by: disposeBag)
    }
    private func setupValidationMessages() {
        viewModel.validationErrorMessage.asDriver(onErrorJustReturn: nil)
            .filter { $0 != nil }
            .map { $0! }
            .drive(onNext: { [weak self] errorMessage in
                self?.showWarningAlert(errorMessage)
            }).disposed(by: disposeBag)
    }
    // MARK: - Keyboard Notification
    private func setupKeyboardNotifications() {
        NotificationCenter.default.rx.notification(.UIKeyboardWillShow)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillShow(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(.UIKeyboardDidHide)
            .takeUntil(rx.methodInvoked(#selector(viewWillDisappear(_:))))
            .subscribe(onNext: { [weak self] notification in
                KeyboardNotificationManager.shared.keyboardWillHide(notification, scrollView: self?.scrollView)
            }).disposed(by: disposeBag)
    }
}
