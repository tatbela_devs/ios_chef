//
//  HomeViewController.swift
//  Tatbela - Chief
//
//  Created by Daniel Tadrous on 9/15/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController {
    @IBOutlet weak var navigationBar: HomeNavigationBarView!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }

    func setupNavigationBar() {
        navigationBar.title = "Home"
        navigationBar.leftButtonImage = #imageLiteral(resourceName: "BurgerIcon")
        navigationBar.rightButtonImage = nil
        // handle side menu
        navigationBar.leftButton.rx
            .tap
            .asDriver()
            .drive(onNext: {
                let vc = UIStoryboard.sidemenu.instantiateInitialViewController()
                self.present(vc!, animated: true, completion: nil)
            }).disposed(by: disposeBag)
        // handle filter
        navigationBar.rightButton.rx
            .tap
            .asDriver()
            .drive(onNext: {
               
            }).disposed(by: disposeBag)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true;
    }
}
