//
//  DateExtension.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

extension Date{
    
    func getFormattedString(format: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        //dateFormatterPrint.locale = Locale(identifier: L10n.Lang)
        return dateFormatterPrint.string(from: self)
    }
}
