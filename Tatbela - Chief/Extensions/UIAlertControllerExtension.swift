//
//  UIAlertControllerExtension.swift
//  Tatbela - Chief
//
//  Created by Kamal El-Shazly on 9/21/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func initTatbelaAlert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let titleAttributedString = NSAttributedString(string: title, attributes: [
            .font : UIFont(name: Font.medium.rawValue, size: 20)!,
            .foregroundColor : UIColor(hex: 0x88B162)
            ])
        alert.setValue(titleAttributedString, forKey: "attributedTitle")
        let messageAttributedString = NSAttributedString(string: message, attributes: [
            .font : UIFont(name: Font.regular.rawValue, size: 16)!,
            .foregroundColor : UIColor(hex: 0xBCBCBC)
            ])
        alert.setValue(messageAttributedString, forKey: "attributedMessage")
        return alert
    }
}
