//
//  UIStoryBoardExtension.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    static var intro: UIStoryboard {
        return UIStoryboard(name: "Intro", bundle: nil)
    }
    static var login: UIStoryboard {
        return UIStoryboard(name: "Login", bundle: nil)
    }
    static var register: UIStoryboard {
        return UIStoryboard(name: "Register", bundle: nil)
    }
    static var confirmPin: UIStoryboard {
        return UIStoryboard(name: "ConfirmPin", bundle: nil)
    }
    static var forgotPassword: UIStoryboard {
        return UIStoryboard(name: "ForgotPassword", bundle: nil)
    }
    static var bankAccount: UIStoryboard {
        return UIStoryboard(name: "BankAccount", bundle: nil)
    }
    static var reviewRegisteration: UIStoryboard {
        return UIStoryboard(name: "ReviewRegisteration", bundle: nil)
    }
    static var registerationDone: UIStoryboard {
        return UIStoryboard(name: "RegisterationDone", bundle: nil)
    }
    static var home: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    static var sidemenu: UIStoryboard {
        return UIStoryboard(name: "SideMenu", bundle: nil)
    }
    static var profile: UIStoryboard {
        return UIStoryboard(name: "MyProfile", bundle: nil)
    }
    static var mainProfile: UIStoryboard {
        return UIStoryboard(name: "MyProfileMain", bundle: nil)
    }
    static var myMenu: UIStoryboard {
        return UIStoryboard(name: "MyMenu", bundle: nil)
    }
}
