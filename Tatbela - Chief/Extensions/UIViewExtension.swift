//
//  UIViewExtension.swift
//  Tatbela
//
//  Created by Daniel on 8/1/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    
    
    func addShadow(){
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowRadius = 8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        //self.shadowLayer.layer.shadowPath = UIBezierPath(roundedRect: self.frame, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
