//
//  StringExtension.swift
//  Tatbela
//
//  Created by Daniel Tadrous on 8/10/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    func getDate(format:String)->Date?{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format
        return dateFormatterGet.date(from:self)
    }
}
