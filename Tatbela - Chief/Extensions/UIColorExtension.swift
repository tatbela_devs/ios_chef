//
//  UIColorExtension.swift
//  Tatbela
//
//  Created by Kamal El-Shazly on 7/13/18.
//  Copyright © 2018 Kamal El-Shazly. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1) {
        let mask = 0x000000FF
        let r = CGFloat((hex >> 16) & mask) / 255
        let g = CGFloat((hex >> 8) & mask) / 255
        let b = CGFloat(hex & mask) / 255
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    convenience init(hex: String, alpha: CGFloat = 1) {
        func intFromHexString(hexStr: String) -> UInt32 {
            var hexInt: UInt32 = 0
            // Create scanner
            let scanner: Scanner = Scanner(string: hexStr)
            // Tell scanner to skip the # character
            scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
            // Scan hex value
            scanner.scanHexInt32(&hexInt)
            return hexInt
        }
        let hexint = Int(intFromHexString(hexStr: hex))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    static var grey5: UIColor { return UIColor(hex: 0xD1D1D1) }
    static var black1: UIColor { return UIColor(hex: 0x191818) }
    
}
